import React from 'react'
import {Switch,Route} from 'react-router-dom'
import Header from './Header'
import Sidebar from './Sidebar'
import home from './content/home'
import biodata from './content/biodata/biodata'
import list from './content/project/list'
import {Modal,ModalBody,ModalFooter,ModalHeader,Button, Container, Row, Col,Card, Collapse,CardTitle, CardText} from 'reactstrap'

class Dashboard extends React.Component {
    render(){
        return (
            <div>
                <Header />
                <div className="container-fluid">
                    <div className="row">
                        <div style={{color:'#000066'}} class="col-md-9  col-lg-10 pt-3 px-4">
                            <h3>Xsis 2.0</h3>
                            <Row><Col xs="2">
                            <nav class="mt-2">
                <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">

                  <li class="nav-item has-treeview menu-open">

                    <ul style={{color:'#000066'}}class="nav nav-treeview">
                      <li class="nav-item">
                        <a href="/home" class="nav-link">
                          <i class="far fa-circle nav-icon"></i>
                          <p>Home</p>
                        </a>
                      </li>
                      <li class="nav-item">
                        <a href="/biodata" class="nav-link">
                          <i class="far fa-circle nav-icon"></i>
                          <p>List Biodata</p>
                        </a>
                      </li>
                      <li class="nav-item">
                        <a href="/user" class="nav-link">
                          <i class="far fa-circle nav-icon"></i>
                          <p>List User</p>
                        </a>
                      </li>
                    </ul>
                  </li>
                 
                </ul>
              </nav>
              <nav class="mt-2">
                <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">

                  <li class="nav-item has-treeview menu-close">
                    <a href="#" class="nav-link">
                      <p>
                        Project
                        <i class="right fas fa-angle-left"></i>
                      </p>
                    </a>
                    <ul class="nav nav-treeview">
                      <li class="nav-item">
                        <a href="/list" class="nav-link">
                          <i class="far fa-circle nav-icon"></i>
                          <p>List</p>
                        </a>
                      </li>
                    </ul>
                  </li>
                 
                </ul>
              </nav>
              </Col><Col size="lg">

              
                            <Switch>
                                <Route path ="/home" component={home}/>
                                <Route path ="/biodata" component={biodata}/>
                                <Route path ="/list" component={list}/>
                            </Switch></Col></Row>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}
export default Dashboard