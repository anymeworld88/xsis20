import React from 'react'
import {Modal,ModalBody,ModalFooter,ModalHeader,Button, Container, Row, Col,Card, Collapse,CardTitle, CardText} from 'reactstrap'
import axios from 'axios'
import apiconfig from '../../../configs/api.configs.json'
import ViewBiodata2 from './viewbiodata2'
import EditBiodata from'./editBiodata'
import Profile from './profile'
class viewBiodata extends React.Component{
  constructor(props){
    super(props)
    let userdata = JSON.parse(localStorage.getItem(apiconfig.LS.USERDATA))

    this.state={
        formdata:{
            fullname:'',
            fullname2:''
        },
        currentBiodata:{},
        editBiodata:false,
        biodata:false,
        profile:false,
        viewBiodata : false
    }
this.BiodataButton = this.BiodataButton.bind(this)
this.ProfileButton = this.ProfileButton.bind(this)

  }

  closeHandler(){
    this.setState({
        editBiodata : false
        
    })
    this.props.getlist()
    this.props.getlist()
    this.props.getlist()
    this.props.getlist()
    this.props.getlist()
    
}

BiodataButton(){
  this.setState({
    biodata : true,
    profile:false
  })
}
ProfileButton(){
  this.setState({
    biodata : false,
    profile:true
  })
}

popupHandler(){
  this.setState({
      viewBiodata : false,
      editBiodata:true   
  })
  this.getListBiodata()
}
    render(){
        return(
            <Modal style={{color:'#000066'}}isOpen={this.props.view} className={this.props.className} size='lg'>
                <ModalHeader style={{color:'#ffffff', backgroundColor:'#000066'}}><Container><Row><Col size="lg" xs="6" style={{columnWidth:'700px'}}>Detail Pelamar</Col><Col size="sm"><Button style={{color:'#ffffff',float:'right'}}close onClick={this.props.closeModalHandler} class="rounded float-right"/></Col></Row></Container></ModalHeader>
                <ModalBody>
                    <Container>
                        <Row>
                            <Col xs="3">
                            <div>
      <Button outline color="secondary" size="sm" block onClick={this.ProfileButton}>Profile</Button>{' '}</div>
      <div>
      <Button outline color="secondary" size="sm" block onClick={this.BiodataButton}>Biodata</Button>{' '}</div>
      <div>
      <Button outline color="secondary" size="sm" block >Pengalaman Kerja</Button>{' '}</div>
      <div>
      <Button outline color="secondary" size="sm" block>Pendidikan</Button>{' '}</div>
      <div>
      <Button outline color="secondary" size="sm" block>Pelatihan</Button>{' '}</div>
      <div>
      <Button outline color="secondary" size="sm" block>Sertifikasi</Button>{' '}</div>
      <div>
      <Button outline color="secondary" size="sm" block>Sumber Lowongan Kerja</Button>{' '}</div>
      <div>
      <Button outline color="secondary" size="sm" block>Organisasi</Button>{' '}</div>
      <div>
      <Button outline color="secondary" size="sm" block>Keluarga</Button>{' '}</div>
      <div>
      <Button outline color="secondary" size="sm" block>Keahlian</Button>{' '}</div>
      <div>
      <Button outline color="secondary" size="sm" block>Lain - Lain</Button>{' '}</div>
      <div>
      <Button outline color="secondary" size="sm" block>Dokumen</Button>{' '}</div>
      <div>
      <Button outline color="secondary" size="sm" block>Catatan</Button>{' '}</div>
      <div>
      <Button outline color="secondary" size="sm" block>Aktivasi Akun</Button>{' '}</div>
      <div>
      <Button outline color="secondary" size="sm" block>Lihat Tes</Button>{' '}</div>
      <div>
      <Button outline color="secondary" size="sm" block>Hasil Tes</Button>{' '}</div>

    </Col>
    <Col size="lg">
    <Collapse isOpen={this.state.biodata}>
          <ViewBiodata2
              view = {this.state.biodata}
              edits ={this.state.editBiodata}
               closeModalHandler =    {this.props.closeModalHandler}
                popupHandler =    {this.props.popupHandler}
                 biodata =    {this.props.biodata}
                 piew = {this.props.piew}
                 getlist =   {this.props.getlist}>
          </ViewBiodata2>
      </Collapse>
      <Collapse isOpen={this.state.profile}>
      <Profile 
      profile = {this.state.profile}
      biodata = {this.props.biodata}/>
      </Collapse>
    </Col>
    </Row>
    </Container>
                </ModalBody>
                <ModalFooter>
                </ModalFooter>
            </Modal>
        )
    }
}
export default viewBiodata