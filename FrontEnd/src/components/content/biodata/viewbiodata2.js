import React from 'react'
import {Modal,ModalBody,ModalFooter,ModalHeader,Button, Container, Row, Col,Card, Collapse,CardTitle, CardText,Label} from 'reactstrap'
import axios from 'axios'
import apiconfig from '../../../configs/api.configs.json'
import {Link} from 'react-router-dom'
import EditBiodata from './editBiodata'
import ViewBiodata from './viewBiodata'
class ViewBiodata2 extends React.Component{
    constructor(props){
        super(props)
        let userdata = JSON.parse(localStorage.getItem(apiconfig.LS.USERDATA))
    
        this.state={
            view:[],view2:[],
            formdata:{},yosdata:{},
            currentBiodata:{},Religion:[],Marital:[],Identity:[],
            tahun:'',
            editBiodata:false,
            biodata:false,
            profile:false,
            viewBiodata:false
        }
        this.editModalHandler1 = this.editModalHandler1.bind(this)
        this.popup = this.popup.bind(this)
        this.closeHandler = this.closeHandler.bind(this)
        this.closeAjjah = this.closeAjjah.bind(this)

    }
    componentWillReceiveProps(newProps){
      this.setState({
          yosdata:newProps.biodata,

      })

  }
    viewModalHandler(kodebiodata){
        let tmp = {}
        this.state.view2.map((row)=>{
            if(kodebiodata == row.idbener){
                tmp = row
            }
        })
        this.setState({
            currentBiodata : tmp,
            editBiodata:true   
        })
        this.getListView2()
    }
    closeHandler(){
        this.setState({
            editBiodata:false,
            yosdata: this.state.currentBiodata
        })
        this.getListView2()
  
    }
    closeAjjah(){
      this.setState({
        editBiodata:false
      })
      this.getListView2()
    }

    getListView2(){
      let token = localStorage.getItem(apiconfig.LS.TOKEN)
      let option = {
          url: apiconfig.BASE_URL+apiconfig.ENDPOINTS.BIODATA2,
          method:"put",
          header:{
              "Authorization": token
          }, data:this.props.biodata
      }
      axios(option).then((response)=>{
          if(response.data.code ==200){
              this.setState({
                  view2: response.data.message
              }) 

          }else{
              alert(response.data.message)
          }
        
      }).catch((error)=>{
          alert(error)
      })
    }
    Tahun(){
      var a = this.props.biodata.dob
      var b = a.split('-')
      var c = b[2]+'-'+b[1]+'-'+b[0]
      this.setState({
        Tahun:c
      })
    }
    componentDidMount(){
        this.getListView2()
        this.Tahun()
        this.getListReligion()
        this.getListMarital()
        this.getListIdentity()
        this.gender()
    }
    getListMarital(){
      let token = localStorage.getItem(apiconfig.LS.TOKEN)
    let option = {
        url: apiconfig.BASE_URL+apiconfig.ENDPOINTS.MARITAL,
        method:"get",
        header:{
            "Authorization": token
        }
    }
    axios(option).then((response)=>{
        if(response.data.code ==200){
          let tmp = response.data.message
          this.setState({
            Marital: tmp     
          })
        }else{
            alert(response.data.message)
        }
    }).catch((error)=>{
        alert(error)
    })
  }
  getListIdentity(){
      let token = localStorage.getItem(apiconfig.LS.TOKEN)
    let option = {
        url: apiconfig.BASE_URL+apiconfig.ENDPOINTS.IDENTITY,
        method:"get",
        header:{
            "Authorization": token
        }
    }
    axios(option).then((response)=>{
        if(response.data.code ==200){
          let tmp = response.data.message
          this.setState({
            Identity: tmp     
          })
        }else{
            alert(response.data.message)
        }
    }).catch((error)=>{
        alert(error)
    })
  }
    getListReligion(){
      let token = localStorage.getItem(apiconfig.LS.TOKEN)
    let option = {
        url: apiconfig.BASE_URL+apiconfig.ENDPOINTS.RELIGION,
        method:"get",
        header:{
            "Authorization": token
        }
    }
    axios(option).then((response)=>{
        if(response.data.code ==200){
          let tmp = response.data.message
          this.setState({
            Religion: tmp     
          })
        }else{
            alert(response.data.message)
        }
    }).catch((error)=>{
        alert(error)
    })
  }
    editModalHandler1(kodebiodata){
        this.state.view.map((row)=>{
            if(kodebiodata == row.id){
                this.setState({
                    currentBiodata : row,
                    editBiodata:true
                }) 
            }
        })
    }
    popup(){
        this.setState({
            editBiodata:true   
        })
    }
  // status(){
  //   if(this.props.biodata.marital_status_id == 1){ this.state.yosdata.status = 'Single'}
  //   if(this.props.biodata.marital_status_id == 2){ this.state.yosdata.status = 'Menikah'}
  // }
  // jenis(){
  //   if(this.props.biodata.identity_type_id == 1){ this.state.yosdata.identitas='KTP'}
  //   if(this.props.biodata.identity_type_id == 2){ this.state.yosdata.identitas='SIM'}
  // }
  // agama(){
  //   if(this.props.biodata.religion_id == 1){ this.state.yosdata.agama='Hindu'}
  //   if(this.props.biodata.religion_id == 2){ this.state.yosdata.agama='Islam'}
  //   if(this.props.biodata.religion_id == 3){ this.state.yosdata.agama='Khatolik'}
  //   if(this.props.biodata.religion_id == 4){ this.state.yosdata.agama='Protestan'}
  //   if(this.props.biodata.religion_id == 5){ this.state.yosdata.agama='Budha'}
  // }
  gender(){

    if(this.state.yosdata.gender == true){ this.state.yosdata.jk='Pria' }
    else if(this.state.yosdata.gender == false){ this.state.yosdata.jk='Perempuan'}
    else if(this.state.yosdata.gender == 'false'){ this.state.yosdata.jk='Perempuan'}
    else if(this.state.yosdata.gender == 'true'){ this.state.yosdata.jk='Pria'}
  }
    render(){
      this.gender()
        return(
            <Card body outline color="secondary" isOpen={this.props.view} className={this.props.className} size="lg" >
            <CardTitle style={{borderBottomStyle:'ridge', borderBottomColor:'#000066'}}> <table id="mytable" class="table table-bordered table-striped">
                 <thead>
                     <tr>
                         </tr></thead> 
                         <tbody>
                           <td>Biodata</td>
                             {
                                this.state.view2.map((row,x)=><Link to='#'>
                                    <td onClick={()=>{this.viewModalHandler(row.idbener)} } class="fa fa-edit">

                                            </td>  </Link>)
                             }</tbody>  
                </table></CardTitle> 
        <CardText> <Row style={{borderBottomStyle:'ridge', borderBottomColor:'#000066'}}>
        <Col xs="6">Nama Lengkap</Col>
        <Col xs="auto">:</Col>
        <Col xs="auto" >{ this.state.yosdata.fullname}</Col>
      </Row>
      <Row style={{borderBottomStyle:'ridge', borderBottomColor:'#000066'}}>
        <Col xs="6">Nama Panggilan</Col>
        <Col xs="auto">:</Col>
        <Col xs="auto" >{this.state.yosdata.nick_name}</Col>
      </Row>
      <Row style={{borderBottomStyle:'ridge', borderBottomColor:'#000066'}}>
        <Col xs="6">Kontak</Col>
        <Col xs="auto">:</Col>
        <Col xs="auto" >{this.state.yosdata.email}/{this.state.yosdata.phone_number1}</Col>
      </Row>
      <Row style={{borderBottomStyle:'ridge', borderBottomColor:'#000066'}}>
        <Col xs="6">Tempat,Tgl. Lahir</Col>
        <Col xs="auto">:</Col>
                            <Col xs="auto" >{this.state.yosdata.pob},{this.state.yosdata.dob}</Col>
      </Row>
      <Row style={{borderBottomStyle:'ridge', borderBottomColor:'#000066'}}>
        <Col xs="6">Jenis Kelamin,Tinggi(cm),Berat(Kg)</Col>
        <Col xs="auto">:</Col>
        <Col xs="auto" >{this.state.yosdata.jk},{this.state.yosdata.high},{this.state.yosdata.weight}</Col>
      </Row>
      <Row style={{borderBottomStyle:'ridge', borderBottomColor:'#000066'}}>
        <Col xs="6">Agama</Col>
        <Col xs="auto">:</Col>
        <Col xs="auto" ><select style={{borderStyle:"none",WebkitAppearance:"none"}}
        readOnly
        name="religion_id" 
        value={this.state.yosdata.religion_id}>
            {this.state.Religion.map((row,x)=>
                                <option value={row.id}>{row.name}</option>)
                            }   
                 </select></Col>
      </Row>
      <Row style={{borderBottomStyle:'ridge', borderBottomColor:'#000066'}}> 
        <Col xs="6">Kewarganegaraan, Suku Bangsa</Col>
        <Col xs="auto">:</Col>
        <Col xs="auto" >{this.state.yosdata.nationality},{this.state.yosdata.ethnic}</Col>
      </Row>
      <Row style={{borderBottomStyle:'ridge', borderBottomColor:'#000066'}}>
        <Col xs="6">Kegemaran / Hobby</Col>
        <Col xs="auto">:</Col>
        <Col xs="auto" >{this.state.yosdata.hobby}</Col>
      </Row>
      <Row style={{borderBottomStyle:'ridge', borderBottomColor:'#000066'}}>
        <Col xs="6">Jenis & Nomor Identitas</Col>
        <Col xs="auto">:</Col>
        <Col xs="auto" ><select style={{borderStyle:"none",WebkitAppearance:"none"}}
        readOnly
        name="identity_type_id" 
        value={this.state.yosdata.identity_type_id}>
            {this.state.Identity.map((row,x)=>
                                <option value={row.id}>{row.name}</option>)
                            }   
                 </select> - {this.state.yosdata.identity_no}</Col>
      </Row>
      <Row >
        <Col xs="6">Status Perkawinan - Tahun Menikah</Col>
        <Col xs="auto">:</Col>
        <Col xs="auto" ><select style={{borderStyle:"none",WebkitAppearance:"none"}}
        readOnly
        name="marital_status_id" 
        value={this.state.yosdata.marital_status_id}>
            {this.state.Marital.map((row,x)=>
                                <option value={row.id}>{row.name}</option>)
                            }   
                 </select> - {this.state.yosdata.marriage_year}</Col>
      </Row></CardText>
<EditBiodata edits = {this.state.editBiodata}
                closeHandler = {this.closeHandler}
                closeAjjah = {this.closeAjjah}
                biodatatest = {this.state.currentBiodata}
                Biodata = {this.state.currentBiodata}
                zxc = {this.props.biodata}
                backHandler = {this.backHandler}></EditBiodata>
                <ViewBiodata edit = {this.state.view} lagi = {this.currentBiodata}/>
      </Card>
        )
    }
}
export default ViewBiodata2