import React from 'react'
import {Modal,ModalBody,ModalFooter,ModalHeader,Button,Container,Row,Col,Label,Input,Form,FormGroup} from 'reactstrap'
import axios from 'axios'
import apiconfig from '../../../configs/api.configs.json'

class EditBiodata extends React.Component{
    constructor (props){
        super(props)
        let userdata = JSON.parse(localStorage.getItem(apiconfig.LS.USERDATA))

        this.state = {
            formdata:{
                modified_on:'',
                fullname:'',
                nick_name:'',
                pob:'',
                dob:'',
                gender:'',
                religion_id:'',
                high:'',
                weight:'',
                nationality:'',
                ethnic:'',
                hobby:'',
                identity_type_id:'',
                identity_no:'',
                child_sequence:'',
                how_many_brothers:'',
                marital_status_id:'',
                marriage_year:'',
                id:'',
                fullname2:'',
                nick_name2:'',
                pob2:'',
                dob2:'',
                gender2:'',
                religion_id2:'',
                high2:'',
                weight2:'',
                nationality2:'',
                ethnic2:'',
                hobby2:'',
                identity_type_id2:'',
                identity_no2:'',
                child_sequence2:'',
                how_many_brothers2:'',
                marital_status_id2:'',
                marriage_year2:'',
                id:'',
                selectedOption: {}
            },
            Tanggal:'',
            nm_mhsError:'',
        EError:'',
        kd_mhsError:'',
        KdError:'',
        jkError:'',
        JError:'',
        alamatError:'',
        AError:'',
        Kd_jurusanError:'',
        KdjError:'',
        statusError:'',
        SError:'',
        NaError:'',
        EtError:'',
        HoError:'',
        NoIError:'',
        AnKeError:'',
        BroError:'',
        TError:'',
        Add1Error:'',
Post1Error:'',
rt1Error:'',
rw1Error:'',
kec1Error:'',
kel1Error:'',
reg1Error:'',
Add2Error:'',
Post2Error:'',
kel2Error:'',
rt2Error:'',
rw2Error:'',
kec2Error:'',
reg2Error:'',
fullError:'',
nickError:'',
pob2Error:'',
highError:'',
weightError:'',
AnKe2Error:'',
Bro2Error:'',
NoI2Error:'',
nationalError:'',
sukuError:'',
EmailError:'',
EmError:'',
Phone1Error:'',
OrtuError:'',
Phone11Error:'',
Ortu1Error:'' ,
Religion:[],
Identity:[],
Marital:[],
Email:[],
NoHp1:[],
NoIdentity1:[]
        }
        this.changeHandler = this.changeHandler.bind(this)
        this.submitHandler = this.submitHandler.bind(this)
        this.Coba =this.Coba.bind(this)
        // this.cancel = this.cancel.bind(this)

    }

    componentWillReceiveProps(newProps){
        this.setState({
            formdata:newProps.biodatatest
        })

    }
validate(){
    let isError =false
    var a = new Date().getFullYear() ;
    var b = this.state.formdata.dob
    var c = b.split('-')
    var f = a - c[0]
    
    const errors = {
        nm_mhsError:'',
        EError:'',
        kd_mhsError:'',
        KdError:'',
        jkError:'',
        JError:'',
        alamatError:'',
        AError:'',
        Kd_jurusanError:'',
        KdjError:'',
        statusError:'',
        SError:'',
        NaError:'',
        EtError:'',
        HoError:'',
        NoIError:'',
        AnKeError:'',
        BroError:'',
        TError:'',
        Add1Error:'',
Post1Error:'',
rt1Error:'',
rw1Error:'',
kec1Error:'',
kel1Error:'',
reg1Error:'',
Add2Error:'',
Post2Error:'',
kel2Error:'',
rt2Error:'',
rw2Error:'',
kec2Error:'',
reg2Error:'',
fullError:'',
nickError:'',
pob2Error:'',
highError:'',
weightError:'',
AnKe2Error:'',
Bro2Error:'',
NoI2Error:'',
nationalError:'',
sukuError:'',
EmailError:'',
EmError:'',
Phone1Error:'',
OrtuError:'',
Phone11Error:'',
Ortu1Error:'',
CheckError:'',
tahunError:''

    };
    if(f < 0){
        isError = true
        errors.tahunError = 'Tahun Melebihi tanggal Sekarang'
    }
    if (this.state.formdata.fullname == ""  ){
        isError= true
        errors.EError = 'Harus Di Isi'
    } else if(!this.state.formdata.fullname.match(/^[a-z A-Z]+$/)){
        isError=true
        errors.fullError = 'Nama Harus Di isi Dengan Huruf'
    }
    if(this.state.formdata.nick_name == ""  ){
        isError=true
        errors.SError = 'Harus Di Isi'
    } else if(!this.state.formdata.nick_name.match(/^[a-z A-Z]+$/)){
        isError=true
        errors.nickError = 'Nama Harus Di isi Dengan Huruf'
    }
    if(this.state.formdata.pob == ""){
        isError = true
        errors.KdjError = 'Harus Di Isi'
    }  
    else if(!this.state.formdata.pob.match(/^[a-z A-Z]+$/)){
        isError=true
        errors.pob2Error = 'Tempat Lahir Harus Di isi Dengan Huruf'
    }
    if(this.state.formdata.dob == ""){
        isError= true
        errors.AError = 'Harus Di Isi'
    }
    if(this.state.formdata.high == "" ){
        isError = true
        errors.JError='Harus Di Isi'
    }
    if(this.state.formdata.weight == ""){
        isError=true
        errors.KdError='Harus Di Isi'
    }
    if(this.state.formdata.nationality == ""){
        isError= true
        errors.NaError ='Harus Di Isi'
    }else if(!this.state.formdata.nationality.match(/^[a-z A-Z]+$/)){
        isError=true
        errors.nationalError = 'Harus Di isi Dengan Huruf'
    }
    if(this.state.formdata.ethnic == "" ){
        isError = true
        errors.EtError='Harus Di Isi'
    }else if(!this.state.formdata.ethnic.match(/^[a-z A-Z]+$/)){
        isError=true
        errors.sukuError = 'Harus Di isi Dengan Huruf'
    }
    if(this.state.formdata.hobby == ""){
        isError=true
        errors.HoError='Harus Di Isi'
    }
    if(this.state.formdata.identity_no == ""){
        isError= true
        errors.NoIError = 'Harus Di Isi'
    } else if(!this.state.formdata.identity_no.match(/^[0-9]+$/)){
        isError=true
        errors.NoI2Error = 'Nomor Identitas Harus Berupa Angka'
    }else {
        this.state.NoIdentity1.map((row)=>{
            if(this.state.formdata.identity_no == row.identity_no){
                isError=true
            errors.CheckNoIdentityError = 'Number Identity Already In Use'
            }
        })}
    if(this.state.formdata.child_sequence == "" ){
        isError = true
        errors.AnKeError='Harus Di Isi'
    } else if(!this.state.formdata.child_sequence.match(/^[0-9999]+$/)){
        isError=true
        errors.AnKe2Error = 'Hanya Bisa Di isi Oleh Angka E.g(1)'
    }
    if(this.state.formdata.how_many_brothers == ""){
        isError=true
        errors.BroError='Harus Di Isi'
    } else if(!this.state.formdata.how_many_brothers.match(/^[0-9999]+$/)){
        isError=true
        errors.Bro2Error = 'Hanya Bisa Di isi Oleh Angka E.g(1)'
    }
    if(this.state.formdata.marital_status_id == 2 && this.state.formdata.marriage_year ==''){
        isError=true
        errors.TError='Harus Di Isi Jika Menikah'
    }
    if(this.state.formdata.marital_status_id == 1 && this.state.formdata.marriage_year !=''){
        isError=true
        errors.TError='Anda Belum Menikah'
    }
    if(this.state.formdata.address1 == ""){
        isError=true
        errors.Add1Error='Harus Di Isi'
    }
    if(this.state.formdata.postal_code1 == ""){
        isError= true
        errors.Post1Error = 'Harus Di Isi'
    }
    if(this.state.formdata.rt1 == "" ){
        isError = true
        errors.rt1Error='Harus Di Isi'
    }
    if(this.state.formdata.rw1 == ""){
        isError=true
        errors.rw1Error='Harus Di Isi'
    }
    if(this.state.formdata.kelurahan1 == ""){
        isError= true
        errors.kel1Error = 'Harus Di Isi'
    }
    if(this.state.formdata.kecamatan1 == "" ){
        isError = true
        errors.kec1Error='Harus Di Isi'
    }
    if(this.state.formdata.region1 == ""){
        isError=true
        errors.reg1Error='Harus Di Isi'
    }
    if(this.state.formdata.address2 == ""){
        isError=true
        errors.Add2Error='Harus Di Isi'
    }
    if(this.state.formdata.postal_code2 == ""){
        isError= true
        errors.Post2Error = 'Harus Di Isi'
    }
    if(this.state.formdata.rt2 == "" ){
        isError = true
        errors.rt2Error='Harus Di Isi'
    }
    if(this.state.formdata.rw2 == ""){
        isError=true
        errors.rw2Error='Harus Di Isi'
    }
    if(this.state.formdata.kelurahan2 == ""){
        isError= true
        errors.kel2Error = 'Harus Di Isi'
    }
    if(this.state.formdata.kecamatan2 == "" ){
        isError = true
        errors.kec2Error='Harus Di Isi'
    }
    if(this.state.formdata.region2 == ""){
        isError=true
        errors.reg2Error='Harus Di Isi'
    }
     if(this.state.formdata.email ==""){
        isError=true
        errors.EmError = 'Email harus Diisi'
    }else if(!this.state.formdata.email.match(/^[a-zA-Z0-9]+@[a-zA-Z0-9]+\.[A-Za-z]+$/)){
        isError=true
        errors.EmailError = 'Email Salah Harus menggunaka @ e.g(punyasaya@example.com'
    }else {
        this.state.Email.map((row)=>{
            if(this.state.formdata.email == row.email){
                isError=true
            errors.CheckError = 'Email Already In Use'
            }
        })}
    if(this.state.formdata.phone_number1 ==""){
        isError=true
        errors.Phone1Error = 'No Hp harus Diisi'
    }else if(!this.state.formdata.phone_number1.match(/^[0-9]+$/)){
        isError=true
        errors.Phone11Error = 'Nomor Hp Harus Menggunakan Angka'
    }else {
        this.state.NoHp1.map((row)=>{
            if(this.state.formdata.phone_number1 == row.phone_number1){
                isError=true
            errors.CheckPhoneError = 'Number Phone Already In Use'
            }
        })}
    if(this.state.formdata.parent_phone_number ==""){
        isError=true
        errors.OrtuError = 'No Hp orang Tua harus Diisi'
    }
    else if(!this.state.formdata.parent_phone_number.match(/^[0-9]+$/)){
        isError=true
        errors.Ortu1Error = 'Nomor Hp Harus Menggunakan Angka'
    }



    if(isError){
        this.setState({
            ...this.state.formdata,
            ...errors
        })
    }
    return isError
}

    changeHandler(e){
        let tmp = this.state.formdata
        tmp[e.target.name]=e.target.value
        this.setState({
            formdata:tmp
        })
    }
    Tanggal(){
        var a = new Date().getFullYear() ;
        var aa = new Date().getMonth()+1 ;
        var aaa = new Date().getDate();
        var b = new Date().getHours();
        var bb = new Date().getMinutes();
        var bbb = new Date().getSeconds();
        var c = a+'-'+aa+'-'+aaa+" "+b+":"+bb+":"+bbb
        this.setState({
            Tanggal:c
        })
    }
    componentDidMount(){
        this.Tanggal()
        this.getListReligion()
        this.getListIdentity()
        this.getListMarital()
        this.getListEmail()
        this.getListNoHp1()
        this.getListNoIdentity1()
    }
    getListEmail(){
        let token = localStorage.getItem(apiconfig.LS.TOKEN)
      let option = {
          url: apiconfig.BASE_URL+apiconfig.ENDPOINTS.EMAIL,
          method:"put",
          header:{
              "Authorization": token
          }, data:this.props.zxc
      }
      axios(option).then((response)=>{
          if(response.data.code ==200){
              this.setState({
                  Email: response.data.message
              }) 
          }else{
              alert(response.data.message)
          }
      }).catch((error)=>{
          alert(error)
      })
    }
    getListNoHp1(){
        let token = localStorage.getItem(apiconfig.LS.TOKEN)
      let option = {
          url: apiconfig.BASE_URL+apiconfig.ENDPOINTS.NOHP1,
          method:"put",
          header:{
              "Authorization": token
          }, data:this.props.zxc
      }
      axios(option).then((response)=>{
          if(response.data.code ==200){
              this.setState({
                  NoHp1: response.data.message
              }) 
          }else{
              alert(response.data.message)
          }
      }).catch((error)=>{
          alert(error)
      })
    }
    getListNoIdentity1(){
        let token = localStorage.getItem(apiconfig.LS.TOKEN)
      let option = {
          url: apiconfig.BASE_URL+apiconfig.ENDPOINTS.NOIDENTITY1,
          method:"put",
          header:{
              "Authorization": token
          }, data:this.props.zxc
      }
      axios(option).then((response)=>{
          if(response.data.code ==200){
              this.setState({
                  NoIdentity1: response.data.message
              }) 
          }else{
              alert(response.data.message)
          }
      }).catch((error)=>{
          alert(error)
      })
    }
    getListMarital(){
        let token = localStorage.getItem(apiconfig.LS.TOKEN)
      let option = {
          url: apiconfig.BASE_URL+apiconfig.ENDPOINTS.MARITAL,
          method:"get",
          header:{
              "Authorization": token
          }
      }
      axios(option).then((response)=>{
          if(response.data.code ==200){
            let tmp = response.data.message
            this.setState({
              Marital: tmp     
            })
          }else{
              alert(response.data.message)
          }
      }).catch((error)=>{
          alert(error)
      })
    }
    getListIdentity(){
        let token = localStorage.getItem(apiconfig.LS.TOKEN)
      let option = {
          url: apiconfig.BASE_URL+apiconfig.ENDPOINTS.IDENTITY,
          method:"get",
          header:{
              "Authorization": token
          }
      }
      axios(option).then((response)=>{
          if(response.data.code ==200){
            let tmp = response.data.message
            this.setState({
              Identity: tmp     
            })
          }else{
              alert(response.data.message)
          }
      }).catch((error)=>{
          alert(error)
      })
    }
    getListReligion(){
        let token = localStorage.getItem(apiconfig.LS.TOKEN)
      let option = {
          url: apiconfig.BASE_URL+apiconfig.ENDPOINTS.RELIGION,
          method:"get",
          header:{
              "Authorization": token
          }
      }
      axios(option).then((response)=>{
          if(response.data.code ==200){
            let tmp = response.data.message
            this.setState({
              Religion: tmp     
            })
          }else{
              alert(response.data.message)
          }
      }).catch((error)=>{
          alert(error)
      })
    }
    Coba(){
            this.setState({
                nm_mhsError:'',
        EError:'',
        kd_mhsError:'',
        KdError:'',
        jkError:'',
        JError:'',
        alamatError:'',
        AError:'',
        Kd_jurusanError:'',
        KdjError:'',
        statusError:'',
        SError:'',
        NaError:'',
        EtError:'',
        HoError:'',
        NoIError:'',
        AnKeError:'',
        BroError:'',
        TError:'',
        Add1Error:'',
Post1Error:'',
rt1Error:'',
rw1Error:'',
kec1Error:'',
kel1Error:'',
reg1Error:'',
Add2Error:'',
Post2Error:'',
kel2Error:'',
rt2Error:'',
rw2Error:'',
kec2Error:'',
reg2Error:'',
fullError:'',
nickError:'',
pob2Error:'',
highError:'',
weightError:'',
AnKe2Error:'',
Bro2Error:'',
NoI2Error:'',
nationalError:'',
sukuError:'',
EmailError:'',
EmError:'',
Phone1Error:'',
OrtuError:'',
Phone11Error:'',
Ortu1Error:'',
CheckError:'',
tahunError:'',
CheckPhoneError:'',
CheckNoIdentityError:''
            })
        this.props.closeAjjah()
    }
    submitHandler(e){
        e.preventDefault()
        const err = this.validate()
        if(!err){
            this.state.formdata.modified_on=this.state.Tanggal
        let token = localStorage.getItem(apiconfig.LS.TOKEN)
        let option = {
            url: apiconfig.BASE_URL+apiconfig.ENDPOINTS.EDITBIODATA,
            method :"put",
            headers:{
                "Authorization":token,
                "Content-Type" : "application/json"
            },
            data: this.state.formdata
        }

        let option2 = {
            url: apiconfig.BASE_URL+apiconfig.ENDPOINTS.EDITALAMAT,
            method :"put",
            headers:{
                "Authorization":token,
                "Content-Type" : "application/json"
            },
            data: this.state.formdata
        }

        axios(option).then((response)=>{
            if(response.data.code ==200){
                this.props.history.push('/dashboard')

            }else{
                alert(response.data.message)
            }
        }).catch((error)=>{
            console.log(error)
        })

        axios(option2).then((response)=>{
            if(response.data.code ==200){
                alert('Success')
                this.props.history.push('/dashboard')

            }else{
                alert(response.data.message)
            }
        }).catch((error)=>{
            console.log(error)
        })
        this.setState({
            nm_mhsError:'',
        EError:'',
        kd_mhsError:'',
        KdError:'',
        jkError:'',
        JError:'',
        alamatError:'',
        AError:'',
        Kd_jurusanError:'',
        KdjError:'',
        statusError:'',
        SError:'',
        NaError:'',
        EtError:'',
        HoError:'',
        NoIError:'',
        AnKeError:'',
        BroError:'',
        TError:'',
        Add1Error:'',
Post1Error:'',
rt1Error:'',
rw1Error:'',
kec1Error:'',
kel1Error:'',
reg1Error:'',
Add2Error:'',
Post2Error:'',
kel2Error:'',
rt2Error:'',
rw2Error:'',
kec2Error:'',
reg2Error:'',
fullError:'',
nickError:'',
pob2Error:'',
highError:'',
weightError:'',
AnKe2Error:'',
Bro2Error:'',
NoI2Error:'',
nationalError:'',
sukuError:'',
EmailError:'',
EmError:'',
Phone1Error:'',
OrtuError:'',
Phone11Error:'',
Ortu1Error:'',
CheckError:'',
tahunError:'',
CheckPhoneError:'',
CheckNoIdentityError:''
        })
        this.props.closeHandler()
        }
}
    render(){
        
        return(
            <Modal style={{color:'#000066' }} isOpen={this.props.edits} className={this.props.className} size="lg">
                
            <ModalHeader style={{color:'#ffffff', backgroundColor:'#000066'}}> Ubah Biodata </ ModalHeader>
            <ModalBody>
                <Container>
                <Row>
                    <Col>
           <Form>
               <FormGroup>
                    <Label for="text" style={{fontSize:'11px',fontStyle:'italic'}}>Nama Lengkap * </Label>
                    <Input type="text" class="form-control"
                    name="fullname"
                    value={this.state.formdata.fullname}
                    onChange={this.changeHandler}
                    reqiured placeholder="Nama lengkap"/>
                    <Label for="text" style={{color:'#ff0000',fontSize:'14px',fontStyle:'italic'}}>{this.state.EError}</Label>
                    <Label for="text" style={{color:'#ff0000',fontSize:'14px',fontStyle:'italic'}}>{this.state.fullError}</Label>
                    </FormGroup></Form></Col>
                    <Col>
                    <Form>
               <FormGroup>
                    <Label for="text" style={{fontSize:'11px',fontStyle:'italic'}}>Nama Panggilan * </Label>
                    <Input type="text" class="form-control"
                    name="nick_name"
                    value={this.state.formdata.nick_name}
                    onChange={this.changeHandler}
                    reqiured placeholder="Nama Panggilan"/>
                    <Label for="text" style={{color:'#ff0000',fontSize:'14px',fontStyle:'italic'}}>{this.state.SError}</Label>
                    <Label for="text" style={{color:'#ff0000',fontSize:'14px',fontStyle:'italic'}}>{this.state.nickError}</Label>
                    </FormGroup></Form>
                    </Col></Row>
                    <Row>
                        <Col>
                            <Form>
                                <FormGroup>
                    <Label for="text" style={{fontSize:'11px',fontStyle:'italic'}}>Tempat Lahir * </Label>
                    <Input type="text" class="form-control" dateformat
                    name="pob"
                    value={this.state.formdata.pob}
                    onChange={this.changeHandler}
                    reqiured placeholder="Tempat Lahir"/>
                     <Label for="text" style={{color:'#ff0000',fontSize:'14px',fontStyle:'italic'}}>{this.state.KdjError}</Label>
                     <Label for="text" style={{color:'#ff0000',fontSize:'14px',fontStyle:'italic'}}>{this.state.pob2Error}</Label>
                                </FormGroup>
                            </Form>
                        </Col>
                    <Col><Form>
               <FormGroup>
                    <Label for="text" style={{fontSize:'11px',fontStyle:'italic'}}>Tgl. Lahir (yyy-mm-dd) * </Label>
                    <Input type="date" class="form-control" 
                    name="dob"
                    value={this.state.formdata.dob}
                    onChange={this.changeHandler}
                    className="fa fa-calendar"
                   />
                     <Label for="text" style={{color:'#ff0000',fontSize:'14px',fontStyle:'italic'}}>{this.state.AError}</Label>
                     <Label for="text" style={{color:'#ff0000',fontSize:'14px',fontStyle:'italic'}}>{this.state.tahunError}</Label>
                    </FormGroup></Form></Col>
                    <Col><Form>
               <FormGroup>
               <Label for="text" style={{fontSize:'11px',fontStyle:'italic'}}>Jenis Kelamin * </Label>
               <select 
        className="form-control" 
        name="gender" 
        onChange={this.changeHandler}
        value={this.state.formdata.gender}>
            <option value={false}>Perempuan</option>
             <option value={true}>Pria</option>        
                 </select>
                    </FormGroup></Form></Col>
                    <Col><Form>
               <FormGroup>
               <Label for="text" style={{fontSize:'11px',fontStyle:'italic'}}>Agama * </Label>
               <select 
        className="form-control" 
        name="religion_id" 
        onChange={this.changeHandler}
        value={this.state.formdata.religion_id}>
            <option value=''>Pilih</option>
            {this.state.Religion.map((row,x)=>
                                <option value={row.id}>{row.name}</option>)
                            }   
                 </select>
                    </FormGroup></Form></Col>
                    </Row>
                    
                    <Row>
                        <Col>
                            <Form>
                                <FormGroup>
                    <Label for="text" style={{fontSize:'11px',fontStyle:'italic'}}>Tinggi (cm) * </Label>
                    <Input type="text" class="form-control"
                    name="high"
                    value={this.state.formdata.high}
                    onChange={this.changeHandler}
                    reqiured placeholder="Tinggi Badan"/>
                     <Label for="text" style={{color:'#ff0000',fontSize:'14px',fontStyle:'italic'}}>{this.state.JError}</Label>
                                </FormGroup>
                            </Form>
                        </Col>
                    <Col><Form>
               <FormGroup>
                    <Label for="text" style={{fontSize:'11px',fontStyle:'italic'}}>Berat (Kg) * </Label>
                    <Input type="text" class="form-control"
                    name="weight"
                    value={this.state.formdata.weight}
                    onChange={this.changeHandler}
                    reqiured placeholder="Berat Badan"/>
                     <Label for="text" style={{color:'#ff0000',fontSize:'14px',fontStyle:'italic'}}>{this.state.KdError}</Label>
                    </FormGroup></Form></Col>
                    <Col><Form>
               <FormGroup>
                    <Label for="text" style={{fontSize:'11px',fontStyle:'italic'}}>Kewarganegaraan * </Label>
                    <Input type="text" class="form-control"
                    name="nationality"
                    value={this.state.formdata.nationality}
                    onChange={this.changeHandler}
                    reqiured placeholder="Kewarganegaraan"/>
                     <Label for="text" style={{color:'#ff0000',fontSize:'14px',fontStyle:'italic'}}>{this.state.NaError}</Label>
                     <Label for="text" style={{color:'#ff0000',fontSize:'14px',fontStyle:'italic'}}>{this.state.nationalError}</Label>
                    </FormGroup></Form></Col>
                    <Col><Form>
               <FormGroup>
                    <Label for="text" style={{fontSize:'11px',fontStyle:'italic'}}>Suku Bangsa  </Label>
                    <Input type="text" class="form-control"
                    name="ethnic"
                    value={this.state.formdata.ethnic}
                    onChange={this.changeHandler}
                    reqiured placeholder="Suku Bangsa"/>
                     <Label for="text" style={{color:'#ff0000',fontSize:'14px',fontStyle:'italic'}}>{this.state.EtError}</Label>
                     <Label for="text" style={{color:'#ff0000',fontSize:'14px',fontStyle:'italic'}}>{this.state.sukuError}</Label>
                    </FormGroup></Form></Col>

                    </Row>

                    <Row>
                        <Col xs="6">
                            <Form>
                                <FormGroup>
                    <Label for="text" style={{fontSize:'11px',fontStyle:'italic'}}>Kegemaran / Hobby  </Label>
                    <Input type="text" class="form-control"
                    name="hobby"
                    value={this.state.formdata.hobby}
                    onChange={this.changeHandler}
                    reqiured placeholder="Kegemaran / Hobby"/>
                     <Label for="text" style={{color:'#ff0000',fontSize:'14px',fontStyle:'italic'}}>{this.state.HoError}</Label>
                                </FormGroup>
                            </Form>
                        </Col>
                    <Col><Form>
               <FormGroup>
               <Label for="text" style={{fontSize:'11px',fontStyle:'italic'}}>Jenis Identitas * </Label>
               <select 
            className="form-control" 
            name="identity_type_id" 
            onChange={this.changeHandler}
            value={this.state.formdata.identity_type_id}>
             <option value=''>Pilih</option>
                {this.state.Identity.map((row,x)=>
                                <option value={row.id}>{row.name}</option>)
                            }   
         
                 </select>
                    </FormGroup></Form></Col>
                    <Col><Form>
               <FormGroup>
                    <Label for="text" style={{fontSize:'11px',fontStyle:'italic'}}>Nomor Identitas </Label>
                    <Input type="text" class="form-control"
                    name="identity_no"
                    value={this.state.formdata.identity_no}
                    onChange={this.changeHandler}
                    reqiured placeholder="Nomor Identitas"/>
                     <Label for="text" style={{color:'#ff0000',fontSize:'14px',fontStyle:'italic'}}>{this.state.NoIError}</Label>
                     <Label for="text" style={{color:'#ff0000',fontSize:'14px',fontStyle:'italic'}}>{this.state.NoI2Error}</Label>
                     <Label for="text" style={{color:'#ff0000',fontSize:'14px',fontStyle:'italic'}}>{this.state.CheckNoIdentityError}</Label>
                    </FormGroup></Form></Col>
                    </Row>
                    <Row>
                        <Col>
                            <Form>
                                <FormGroup>
                    <Label for="text" style={{fontSize:'11px',fontStyle:'italic'}}>Email * </Label>
                    <Input type="text" class="form-control"
                    name="email"
                    value={this.state.formdata.email}
                    onChange={this.changeHandler}
                    reqiured placeholder="Email"/>
                     <Label for="text" style={{color:'#ff0000',fontSize:'14px',fontStyle:'italic'}}>{this.state.EmailError}</Label>
                     <Label for="text" style={{color:'#ff0000',fontSize:'14px',fontStyle:'italic'}}>{this.state.EmError}</Label>
                     <Label for="text" style={{color:'#ff0000',fontSize:'14px',fontStyle:'italic'}}>{this.state.CheckError}</Label>
                                </FormGroup>
                            </Form>
                        </Col>
                    <Col><Form>
               <FormGroup>
                    <Label for="text" style={{fontSize:'11px',fontStyle:'italic'}}>No Hp * </Label>
                    <Input type="text" class="form-control"
                    name="phone_number1"
                    value={this.state.formdata.phone_number1}
                    onChange={this.changeHandler}
                    reqiured placeholder="No Hp"/>
                     <Label for="text" style={{color:'#ff0000',fontSize:'14px',fontStyle:'italic'}}>{this.state.Phone1Error}</Label>
                     <Label for="text" style={{color:'#ff0000',fontSize:'14px',fontStyle:'italic'}}>{this.state.Phone11Error}</Label>
                     <Label for="text" style={{color:'#ff0000',fontSize:'14px',fontStyle:'italic'}}>{this.state.CheckPhoneError}</Label>
                    </FormGroup></Form></Col>
                    <Col><Form>
               <FormGroup>
                    <Label for="text" style={{fontSize:'11px',fontStyle:'italic'}}>No Hp Alternatif </Label>
                    <Input type="text" class="form-control"
                    name="phone_number2"
                    value={this.state.formdata.phone_number2}
                    onChange={this.changeHandler}
                    reqiured placeholder="No Hp ALternatif"/>
                    </FormGroup></Form></Col>
                    <Col><Form>
               <FormGroup>
                    <Label for="text" style={{fontSize:'11px',fontStyle:'italic'}}>No Tlp (Rmh/Orang tua) * </Label>
                    <Input type="text" class="form-control"
                    name="parent_phone_number"
                    value={this.state.formdata.parent_phone_number}
                    onChange={this.changeHandler}
                    reqiured placeholder="No Tlp (Rumah/Orang Tua)"/>
                     <Label for="text" style={{color:'#ff0000',fontSize:'14px',fontStyle:'italic'}}>{this.state.OrtuError}</Label>
                     <Label for="text" style={{color:'#ff0000',fontSize:'14px',fontStyle:'italic'}}>{this.state.Ortu1Error}</Label>
                    </FormGroup></Form></Col>

                    </Row>
                    <Row>
                        <Col>
                            <Form>
                                <FormGroup>
                    <Label for="text" style={{fontSize:'11px',fontStyle:'italic'}}>Anak Ke * </Label>
                    <Input type="text" class="form-control"
                    name="child_sequence"
                    value={this.state.formdata.child_sequence}
                    onChange={this.changeHandler}
                    reqiured placeholder="Anak Ke"/>
                     <Label for="text" style={{color:'#ff0000',fontSize:'14px',fontStyle:'italic'}}>{this.state.AnKeError}</Label>
                     <Label for="text" style={{color:'#ff0000',fontSize:'14px',fontStyle:'italic'}}>{this.state.AnKe2Error}</Label>     
                                </FormGroup>
                            </Form>
                        </Col>
                    <Col><Form>
               <FormGroup>
                    <Label for="text" style={{fontSize:'11px',fontStyle:'italic'}}>Dari Berapa Bersaudara * </Label>
                    <Input type="text" class="form-control"
                    name="how_many_brothers"
                    value={this.state.formdata.how_many_brothers}
                    onChange={this.changeHandler}
                    reqiured placeholder="Berapa Saudara"/>
                     <Label for="text" style={{color:'#ff0000',fontSize:'14px',fontStyle:'italic'}}>{this.state.BroError}</Label>
                     <Label for="text" style={{color:'#ff0000',fontSize:'14px',fontStyle:'italic'}}>{this.state.Bro2Error}</Label>
                    </FormGroup></Form></Col>
                    <Col><Form>
               <FormGroup>
               <Label for="text" style={{fontSize:'11px',fontStyle:'italic'}}>Status Pernikahan * </Label>
               <select 
        className="form-control" 
        name="marital_status_id" 
        onChange={this.changeHandler}
        value={this.state.formdata.marital_status_id}>
            <option value=''>Pilih</option>
            {this.state.Marital.map((row,x)=>
                                <option value={row.id}>{row.name}</option>)
                            }         
                 </select>
                    </FormGroup></Form></Col>
                    <Col><Form>
               <FormGroup>
               <Label for="text" style={{fontSize:'11px',fontStyle:'italic'}}>Tahun Menikah * </Label>
               <select 
        className="form-control" 
        name="marriage_year" 
        onChange={this.changeHandler}
        value={this.state.formdata.marriage_year}>
            <option value=''>-Pilih-</option>
             <option value='2020'>2020</option> 
             <option value='2019'>2019</option>
             <option value='2018'>2018</option>  
             <option value='2017'>2017</option> 
                 </select>
                 <Label for="text" style={{color:'#ff0000',fontSize:'14px',fontStyle:'italic'}}>{this.state.TError}</Label>
                    </FormGroup></Form></Col>
                    </Row>
                    <Row>
                        <Col>
                        <Label for="text" style={{fontSize:'18px',fontStyle:'italic',borderBottomStyle:'ridge',borderBottomColor:'#000066'}}>Alamat Sesuai Kartu Identitas </Label>
                    <Col><Form>
               <FormGroup>
                    <Label for="text" style={{fontSize:'11px',fontStyle:'italic'}}>Alamat * </Label>
                    <Input type="text" class="form-control"
                    name="address1"
                    value={this.state.formdata.address1}
                    onChange={this.changeHandler}
                    reqiured placeholder="Alamat"/>
                    <Label for="text" style={{color:'#ff0000',fontSize:'14px',fontStyle:'italic'}}>{this.state.Add1Error}</Label>
                    </FormGroup></Form></Col>
                        <Row>
                    <Col xs="6">
                            <Form>
                                <FormGroup>
                    <Label for="text" style={{fontSize:'11px',fontStyle:'italic'}}>Kode Pos </Label>
                    <Input type="text" class="form-control"
                    name="postal_code1"
                    value={this.state.formdata.postal_code1}
                    onChange={this.changeHandler}
                    reqiured placeholder="Kode Pos"/>
                    <Label for="text" style={{color:'#ff0000',fontSize:'14px',fontStyle:'italic'}}>{this.state.Post1Error}</Label>
                                </FormGroup>
                            </Form>
                        </Col>
                    <Col><Form>
               <FormGroup>
                    <Label for="text" style={{fontSize:'11px',fontStyle:'italic'}}>RT * </Label>
                    <Input type="text" class="form-control"
                    name="rt1"
                    value={this.state.formdata.rt1}
                    onChange={this.changeHandler}
                    reqiured placeholder="RT"/>
                    <Label for="text" style={{color:'#ff0000',fontSize:'14px',fontStyle:'italic'}}>{this.state.rt1Error}</Label>
                    </FormGroup></Form></Col>
                    <Col><Form>
               <FormGroup>
                    <Label for="text" style={{fontSize:'11px',fontStyle:'italic'}}>RW * </Label>
                    <Input type="text" class="form-control"
                    name="rw1"
                    value={this.state.formdata.rw1}
                    onChange={this.changeHandler}
                    reqiured placeholder="RW"/>
                    <Label for="text" style={{color:'#ff0000',fontSize:'14px',fontStyle:'italic'}}>{this.state.rw1Error}</Label>
                    </FormGroup></Form></Col></Row>

                    <Row>
                    <Col>
                            <Form>
                                <FormGroup>
                    <Label for="text" style={{fontSize:'11px',fontStyle:'italic'}}>Desa / Kelurahan * </Label>
                    <Input type="text" class="form-control"
                    name="kelurahan1"
                    value={this.state.formdata.kelurahan1}
                    onChange={this.changeHandler}
                    reqiured placeholder="Desa / Kelurahan"/>
                    <Label for="text" style={{color:'#ff0000',fontSize:'14px',fontStyle:'italic'}}>{this.state.kel1Error}</Label>
                                </FormGroup>
                            </Form>
                        </Col>
                    <Col><Form>
               <FormGroup>
                    <Label for="text" style={{fontSize:'11px',fontStyle:'italic'}}>Kecamatan * </Label>
                    <Input type="text" class="form-control"
                    name="kecamatan1"
                    value={this.state.formdata.kecamatan1}
                    onChange={this.changeHandler}
                    reqiured placeholder="Kecamatan"/>
                    <Label for="text" style={{color:'#ff0000',fontSize:'14px',fontStyle:'italic'}}>{this.state.kec1Error}</Label>
                    </FormGroup></Form></Col>
                    <Col><Form>
               <FormGroup>
                    <Label for="text" style={{fontSize:'11px',fontStyle:'italic'}}>Kota * </Label>
                    <Input type="text" class="form-control"
                    name="region1"
                    value={this.state.formdata.region1}
                    onChange={this.changeHandler}
                    reqiured placeholder="Kota"/>
                    <Label for="text" style={{color:'#ff0000',fontSize:'14px',fontStyle:'italic'}}>{this.state.reg1Error}</Label>
                    </FormGroup></Form></Col></Row>

                        </Col>
                        <Col>
                        <Label for="text" style={{fontSize:'18px',fontStyle:'italic',borderBottomStyle:'ridge',borderBottomColor:'#000066'}}>Alamat Sesuai Tempat Tinggal Saat Ini </Label>
                        <Col><Form>
               <FormGroup>
                    <Label for="text" style={{fontSize:'11px',fontStyle:'italic'}}>Alamat * </Label>
                    <Input type="text" class="form-control"
                    name="address2"
                    value={this.state.formdata.address2}
                    onChange={this.changeHandler}
                    reqiured placeholder="Alamat"/>
                    <Label for="text" style={{color:'#ff0000',fontSize:'14px',fontStyle:'italic'}}>{this.state.Add2Error}</Label>
                    </FormGroup></Form></Col>
                        <Row>
                    <Col xs="6">
                            <Form>
                                <FormGroup>
                    <Label for="text" style={{fontSize:'11px',fontStyle:'italic'}}>Kode Pos </Label>
                    <Input type="text" class="form-control"
                    name="postal_code2"
                    value={this.state.formdata.postal_code2}
                    onChange={this.changeHandler}
                    reqiured placeholder="Kode Pos"/>
                    <Label for="text" style={{color:'#ff0000',fontSize:'14px',fontStyle:'italic'}}>{this.state.Post2Error}</Label>
                                </FormGroup>
                            </Form>
                        </Col>
                    <Col><Form>
               <FormGroup>
                    <Label for="text" style={{fontSize:'11px',fontStyle:'italic'}}>RT * </Label>
                    <Input type="text" class="form-control"
                    name="rt2"
                    value={this.state.formdata.rt2}
                    onChange={this.changeHandler}
                    reqiured placeholder="RT"/>
                    <Label for="text" style={{color:'#ff0000',fontSize:'14px',fontStyle:'italic'}}>{this.state.rt2Error}</Label>
                    </FormGroup></Form></Col>
                    <Col><Form>
               <FormGroup>
                    <Label for="text" style={{fontSize:'11px',fontStyle:'italic'}}>RW * </Label>
                    <Input type="text" class="form-control"
                    name="rw2"
                    value={this.state.formdata.rw2}
                    onChange={this.changeHandler}
                    reqiured placeholder="RW"/>
                    <Label for="text" style={{color:'#ff0000',fontSize:'14px',fontStyle:'italic'}}>{this.state.rw2Error}</Label>
                    </FormGroup></Form></Col></Row>

                    <Row>
                    <Col>
                            <Form>
                                <FormGroup>
                    <Label for="text" style={{fontSize:'11px',fontStyle:'italic'}}>Desa / Kelurahan * </Label>
                    <Input type="text" class="form-control"
                    name="kelurahan2"
                    value={this.state.formdata.kelurahan2}
                    onChange={this.changeHandler}
                    reqiured placeholder="Desa / Kelurahan"/>
                    <Label for="text" style={{color:'#ff0000',fontSize:'14px',fontStyle:'italic'}}>{this.state.kel2Error}</Label>
                                </FormGroup>
                            </Form>
                        </Col>
                    <Col><Form>
               <FormGroup>
                    <Label for="text" style={{fontSize:'11px',fontStyle:'italic'}}>Kecamatan * </Label>
                    <Input type="text" class="form-control"
                    name="kecamatan2"
                    value={this.state.formdata.kecamatan2}
                    onChange={this.changeHandler}
                    reqiured placeholder="Kecamatan"/>
                    <Label for="text" style={{color:'#ff0000',fontSize:'14px',fontStyle:'italic'}}>{this.state.kec2Error}</Label>
                    </FormGroup></Form></Col>
                    <Col><Form>
               <FormGroup>
                    <Label for="text" style={{fontSize:'11px',fontStyle:'italic'}}>Kota * </Label>
                    <Input type="text" class="form-control"
                    name="region2"
                    value={this.state.formdata.region2}
                    onChange={this.changeHandler}
                    reqiured placeholder="Kota"/>
                    <Label for="text" style={{color:'#ff0000',fontSize:'14px',fontStyle:'italic'}}>{this.state.reg2Error}</Label>
                    </FormGroup></Form></Col></Row>

                        </Col>
                    </Row>
                    </Container></ModalBody>
                    <ModalFooter> 
                <Button color="warning" onClick={this.Coba} >Batal</Button>
                <Button color="primary" onClick={this.submitHandler} label="Submit" >Simpan </Button>
                    </ModalFooter>
                    </Modal>
                    )
    }
} export default EditBiodata