import React from 'react'
import {Modal,ModalBody,ModalFooter,ModalHeader,Button, Container, Row, Col,Card, Collapse,CardTitle, CardText,ListGroup, ListGroupItem, ListGroupItemHeading, ListGroupItemText} from 'reactstrap'
import axios from 'axios'
import apiconfig from '../../../configs/api.configs.json'
import {Link} from 'react-router-dom'
import EditBiodata from './editBiodata'
import ViewBiodata from './viewBiodata'
import jsPDF from 'jspdf'

class Profile extends React.Component{
    constructor (props){
        super(props)
        this.state={
            pendidikan:{},
            tahun:'',
            penerima:[],penerima1:[],
            pekerjaan:[],
            keahlian:[],
            training:[],
            sertifikasi:[],
            foto:[],
            coba:'file/img/Edited.jpg',
            bulan:'',
            hari:'',
            formdata:{}
        }
    }
    componentWillReceiveProps(newProps){
        this.setState({
            pendidikan:newProps.biodata
  
        })
    }

    getListPhotoProfile(){
        let token = localStorage.getItem(apiconfig.LS.TOKEN)
      let option = {
          url: apiconfig.BASE_URL+apiconfig.ENDPOINTS.PHOTOPROFILE,
          method:"put",
          header:{
              "Authorization": token
          }, data:this.props.biodata
      }
      axios(option).then((response)=>{
          if(response.data.code ==200){
              this.setState({
                  foto: response.data.message
              }) 
          }else{
              alert(response.data.message)
          }
      }).catch((error)=>{
          alert(error)
      })
    }

    getListPendidikan(){
        let token = localStorage.getItem(apiconfig.LS.TOKEN)
      let option = {
          url: apiconfig.BASE_URL+apiconfig.ENDPOINTS.PENDIDIKAN,
          method:"put",
          header:{
              "Authorization": token
          }, data:this.props.biodata
      }
      axios(option).then((response)=>{
          if(response.data.code ==200){
              this.setState({
                  penerima: response.data.message
              }) 
          }else{
              alert(response.data.message)
          }
      }).catch((error)=>{
          alert(error)
      })
    }
    getListPendidikan1(){
        let token = localStorage.getItem(apiconfig.LS.TOKEN)
      let option = {
          url: apiconfig.BASE_URL+apiconfig.ENDPOINTS.PENDIDIKAN1,
          method:"put",
          header:{
              "Authorization": token
          }, data:this.props.biodata
      }
      axios(option).then((response)=>{
          if(response.data.code ==200){
              this.setState({
                  penerima1: response.data.message
              }) 
          }else{
              alert(response.data.message)
          }
      }).catch((error)=>{
          alert(error)
      })
    }
    getListPekerjaan(){
        let token = localStorage.getItem(apiconfig.LS.TOKEN)
      let option = {
          url: apiconfig.BASE_URL+apiconfig.ENDPOINTS.PEKERJAAN,
          method:"put",
          header:{
              "Authorization": token
          }, data:this.props.biodata
      }
      axios(option).then((response)=>{
          if(response.data.code ==200){
              this.setState({
                  pekerjaan: response.data.message
              }) 
          }else{
              alert(response.data.message)
          }
      }).catch((error)=>{
          alert(error)
      })
    }
    getListTraining(){
        let token = localStorage.getItem(apiconfig.LS.TOKEN)
      let option = {
          url: apiconfig.BASE_URL+apiconfig.ENDPOINTS.TRAINING,
          method:"put",
          header:{
              "Authorization": token
          }, data:this.props.biodata
      }
      axios(option).then((response)=>{
          if(response.data.code ==200){
              this.setState({
                  training: response.data.message
              }) 
          }else{
              alert(response.data.message)
          }
      }).catch((error)=>{
          alert(error)
      })
    }
    getListSertifikasi(){
        let token = localStorage.getItem(apiconfig.LS.TOKEN)
      let option = {
          url: apiconfig.BASE_URL+apiconfig.ENDPOINTS.SERTIFIKASI,
          method:"put",
          header:{
              "Authorization": token
          }, data:this.props.biodata
      }
      axios(option).then((response)=>{
          if(response.data.code ==200){
              this.setState({
                  sertifikasi: response.data.message
              }) 
          }else{
              alert(response.data.message)
          }
      }).catch((error)=>{
          alert(error)
      })
    }
    getListKeahlian(){
        let token = localStorage.getItem(apiconfig.LS.TOKEN)
      let option = {
          url: apiconfig.BASE_URL+apiconfig.ENDPOINTS.KEAHLIAN,
          method:"put",
          header:{
              "Authorization": token
          }, data:this.props.biodata
      }
      axios(option).then((response)=>{
          if(response.data.code ==200){
              this.setState({
                  keahlian: response.data.message
              }) 
          }else{
              alert(response.data.message)
          }
      }).catch((error)=>{
          alert(error)
      })
    }
    componentDidMount(){
        this.getListPendidikan()
        this.getListPendidikan1()
        this.getListPekerjaan()
        this.date()
        this.getListKeahlian()
        this.getListTraining()
        this.getListSertifikasi()
        this.getListPhotoProfile()
    }

    date(){
        var a = new Date().getFullYear() ;
        var aa = new Date().getMonth()+1 ;
        var aaa = new Date().getDate();
        var b = this.props.biodata.dob
        var c = b.split('-')
        var d = aaa - c[2]
        var e = aa - c[1]
        var f = a - c[0]
        if(d < 0){e--
        }
        if(e< 0){f-- }
        
this.setState({
    tahun: f,


})

    }

    gender(){

        if(this.props.biodata.gender == true){ this.state.formdata.jk='Pria' }
        else{ this.state.formdata.jk='Perempuan'}
      }
//       Download(){
//              var doc = new jsPDF ()
//           var h = document.getElementById('Test1')
//             var x = document.getElementById('Test2')
//             var y = document.getElementById('Test3')
//             var pendidikan1 = document.getElementById('pendidikan1')
//             var pendidikan2 = document.getElementById('pendidikan2')
//             var pengalaman1 = document.getElementById('pengalaman1')
//             var pengalaman2 = document.getElementById('pengalaman2')
//           var imgData = 'dist/img/user2-160x160.jpg' 
//         doc.setFontSize(40);
//         doc.addImage(imgData, 'JPEG', 15, 15, 40, 40); 
//          doc.fromHTML(h,60,15)
//          doc.fromHTML(x,15,60);
//          doc.fromHTML(pendidikan1,15,70)
//          doc.fromHTML(pendidikan2,40,70)
//          doc.fromHTML(y,15,90);
//          doc.fromHTML(pengalaman1,15,100)
//          doc.fromHTML(pengalaman2,40,100)

// doc.save("generated.pdf")
//       }
    render(){
        return(this.gender(),
            <Card  body outline color="secondary" isOpen={this.props.profile} className={this.props.className} size="lg" >
            <CardTitle><Row><Col xs="auto">
            <div class="image" >
                  <img src={this.state.pendidikan.file_path + this.state.pendidikan.file_name} class="img-circle elevation-2" alt="User Image" width="100px"/>
                </div></Col><Col id='Test1'><Row>
                    <Col size="lg" xs="10" >{this.props.biodata.fullname}
        </Col></Row>
        <Row>  <Col size="lg" xs="10" style={{ color:'#666666', fontSize:'12'}} >{this.state.tahun} Tahun</Col></Row>
        <Row>  <Col size="lg" xs="10" style={{ color:'#666666', fontSize:'12'}}>{this.state.formdata.jk}</Col></Row>
    <Row>  <Col size="lg" xs="10" style={{ color:'#666666', fontSize:'12'}}>  {
                                this.state.penerima1.map((row)=>
                                    <td>{row.major},{row.school_name}</td>
                               )
                             }</Col></Row>
        </Col><Col xs="auto"><Button class="rounded float-right" > Unduh</Button></Col>
                </Row>
                <Col style={{fontSize:'14px'}}>Pendidikan  </Col>
                <table id="mytable" class="table table-bordered table-striped">
                         <tbody>
                             {
                                this.state.penerima.map((row,x)=>
                                <tr>
                                    <td style={{fontSize:'12px'}} width="200px">{row.entry_year} - {row.graduation_year}</td>
                                  <td style={{fontSize:'12px'}}><tr>{row.school_name},{row.country}</tr>
                                    <tr>{row.major}</tr>
                                    <tr>{row.gpa}</tr></td>
                                </tr>)
                             }</tbody>  
                </table>
                <Col style={{fontSize:'14px'}}>Pengalaman</Col>
                <table id="mytable2" class="table table-bordered table-striped">
                
                         <tbody>
                             {
                                this.state.pekerjaan.map((row,x)=>
                                <tr>
                                    <td style={{fontSize:'12px'}} width="200px">{row.join_year} - {row.resign_year}</td>
                                    <td style={{fontSize:'12px'}}><tr>{row.last_position}</tr>
                                      <tr>{row.company_name}</tr></td>
                                </tr>)
                             }</tbody>  
                </table>
                <Col style={{fontSize:'14px'}}>Keahlian</Col>
                <table id="mytable3" class="table table-bordered table-striped">
                         <tbody>
                             {
                                this.state.keahlian.map((row,x)=>
                             <tr><td style={{fontSize:'12px'}} width="200px">{row.name}</td>
                                 <td style={{fontSize:'12px'}}>{row.skill_name}</td>
                                 </tr>)
                             }</tbody>  
                </table>
                <Col style={{fontSize:'14px'}}>Training</Col>
                <table id="mytable" class="table table-bordered table-striped">
                         <tbody>
                             {
                                this.state.training.map((row,x)=>
                                <tr>
                                    <td style={{fontSize:'12px'}} width="200px">{row.training_year}</td>
                                  <td style={{fontSize:'12px'}}><tr>{row.training_name}</tr>
                                    <tr>{row.organizer}</tr></td>
                                </tr>)
                             }</tbody>  
                </table>
                <Col style={{fontSize:'14px'}}>Sertifikasi</Col>
                <table id="mytable" class="table table-bordered table-striped">
                         <tbody>
                             {
                                this.state.sertifikasi.map((row,x)=>
                                <tr>
                                    <td style={{fontSize:'12px'}} width="200px">{row.valid_start_year} - {row.until_year}</td>
                                  <td style={{fontSize:'12px'}}><tr>{row.certificate_name}</tr>
                                    <tr>{row.publisher}</tr>
                                    </td>
                                </tr>)
                             }</tbody>  
                </table>
                
                
                </CardTitle>


            </Card>
        )
    }

}
export default Profile