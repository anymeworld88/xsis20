import React from 'react'
import apiconfig from '../../../configs/api.configs.json'
import {Modal,ModalBody,ModalFooter,ModalHeader,Button,Container,Row,Col,Label,Input,Form,FormGroup,ButtonDropdown, DropdownToggle, DropdownMenu, DropdownItem} from 'reactstrap'
import axios from 'axios'
import {Link} from 'react-router-dom'
import Detail from './detail'
import Hapus from './hapus'
import EditList from './edit'
import TambahList from './tambah'
import { Pagination } from 'react-bootstrap';

//import {Alert} from 'reactstrap'

class list extends React.Component{
    constructor(props){
        super(props)
        this.state={
            search1:{start_project:'',end_project:''},
            resource:[],
            currentList:{},viewList:false,
            ascending:false,
            pagenation:false,
            from:false,end:false,more:false ,begin:0,end:10,coba:10,
            editList:false,search:'',search2:'',date:'',hapusList:false ,tambahList:false
        }
        this.dropdownOpen=this.dropdownOpen.bind(this)
        this.dropdownClose=this.dropdownClose.bind(this)
        this.dropdownMore = this.dropdownMore.bind(this)
        this.dropdownMore1 = this.dropdownMore1.bind(this)
        this.dropdownOpen1=this.dropdownOpen1.bind(this)
        this.dropdownClose1=this.dropdownClose1.bind(this)
        this.getListResourceDesc=this.getListResourceDesc.bind(this)
        this.getListResourceAsc=this.getListResourceAsc.bind(this)
        this.viewModalHandler = this.viewModalHandler.bind(this)
        this.closeModalHandler = this.closeModalHandler.bind(this)
        this.hapusModalHandler = this.hapusModalHandler.bind(this)
        this.editModalHandler = this.editModalHandler.bind(this)
        this.tambahModalHandler = this.tambahModalHandler.bind(this)
        this.getListResourceFind = this.getListResourceFind.bind(this)
        this.next = this.next.bind(this)
        this.changeHandler = this.changeHandler.bind(this)
        this.validate = this.validate.bind(this)
    }
    componentDidMount(){
        this.getListResource()
    }
    dropdownMore1(){
        this.setState({
            more:false
        })}
updateSearch(event){
    this.setState({
        search:event.target.value.substr(0,20)
    })
}
changeHandler(e){
    let tmp = this.state.search1
    tmp[e.target.name]=e.target.value
    this.setState({
        search1:tmp
    })
}
dropdownMore(){
    this.setState({
        more:true
    })
}

    dropdownClose(){
        this.setState({
            ascending:false,
            FromError:'',
            UntilError:'' 
        })}
dropdownOpen(){
    this.setState({
        ascending:true
    })
}
dropdownClose1(){
    this.setState({
        pagenation:false,
        FromError:'',
            UntilError:'' 
    })}
dropdownOpen1(){
this.setState({
    pagenation:true
})
}
closeModalHandler(){
    this.setState({
        viewList:false,
        hapusList:false,editList:false,tambahList:false
    })
    this.getListResourceAsc()
    this.getListResourceAsc()
    this.getListResourceAsc()
    this.getListResourceAsc()
    this.getListResourceAsc()
    this.getListResourceAsc()
    this.getListResourceAsc()
    this.getListResourceAsc()
    this.getListResourceAsc()
    this.getListResourceAsc()
    this.getListResourceAsc()
    this.getListResourceAsc()
    this.getListResourceAsc()
    this.getListResourceAsc()
    this.getListResourceAsc()
}
validate(){
    let isError =false
    const errors = {
       FromError:'',
       UntilError:'' 

    };
    if (this.state.search1.start_project == ""  ){
        isError= true
        errors.FromError = 'Harus Di Isi'
    }
    if (this.state.search1.end_project == ""  ){
        isError= true
        errors.UntilError = 'Harus Di Isi'
    }
   


    if(isError){
        this.setState({
            ...this.state.formdata,
            ...errors
        })
    }
    return isError
}
viewModalHandler(kodelist){
    let tmp = {}
    this.state.resource.map((row)=>{
        if(kodelist == row.id){
            tmp = row
        }
    })
    this.setState({
        currentList : tmp,
        viewList:true
    })
    this.getListResource()
}
editModalHandler(kodelist){
    let tmp = {}
    this.state.resource.map((row)=>{
        if(kodelist == row.id){
            tmp = row
        }
    })
    this.setState({
        currentList : tmp,
        editList:true
    })
    this.getListResource()
}
hapusModalHandler(kodelist){
    let tmp = {}
    this.state.resource.map((row)=>{
        if(kodelist == row.id){
            tmp = row
        }
    })
    this.setState({
        currentList : tmp,
        hapusList:true
    })
    this.getListResource()
}
tambahModalHandler(){
    this.setState({
        tambahList:true
    })
    this.getListResource()
}
getListResource(){
    let token = localStorage.getItem(apiconfig.LS.TOKEN)
    let option = {
        url: apiconfig.BASE_URL+apiconfig.ENDPOINTS.RESOURCE,
        method:"get",
        header:{
            "Authorization": token
        }
    }
    axios(option).then((response)=>{
        this.setState({
            resource: response.data.message
        })
    }).catch((error)=>{
        alert(error)
    })
}
getListResourceDesc(){
    let token = localStorage.getItem(apiconfig.LS.TOKEN)
    let option = {
        url: apiconfig.BASE_URL+apiconfig.ENDPOINTS.RESOURCEDESC,
        method:"get",
        header:{
            "Authorization": token
        }
    }
    axios(option).then((response)=>{
        this.setState({
            resource: response.data.message
        })
    }).catch((error)=>{
        alert(error)
    })
}
getListResourceFind(e){
    e.preventDefault()
    const err = this.validate()
    if(!err){
        this.setState({
            FromError:'',
            UntilError:'' 
        })
    let token = localStorage.getItem(apiconfig.LS.TOKEN)
  let option = {
      url: apiconfig.BASE_URL+apiconfig.ENDPOINTS.RESOURCEFIND,
      method:"put",
      header:{
          "Authorization": token
      }, data:this.state.search1
  }
  axios(option).then((response)=>{
      if(response.data.code ==200){
          this.setState({
              resource: response.data.message
          }) 
      }else{
          alert(response.data.message)
      }
  }).catch((error)=>{
      alert(error)
  })
}}
getListResourceAsc(){
    let token = localStorage.getItem(apiconfig.LS.TOKEN)
    let option = {
        url: apiconfig.BASE_URL+apiconfig.ENDPOINTS.RESOURCEASC,
        method:"get",
        header:{
            "Authorization": token
        }
    }
    axios(option).then((response)=>{
        this.setState({
            resource: response.data.message
        })
    }).catch((error)=>{
        alert(error)
    })
}
next(){
    let a = this.state.end
    let b = this.state.coba
    let c = this.state.resource
    let d = c.length
    if(this.state.end < d){
    this.setState({
        begin:a,
        end : a+b
    })}else{ this.setState({
        begin:this.state.begin,
        end:this.state.end
    })}
}
previous(){
    let a = this.state.begin
    let b = this.state.coba
    if(this.state.begin > 0){
    this.setState({
        begin:a-b,
        end:a
    })}else{this.setState({
        begin:this.state.begin,
        end:this.state.end})}
}
rows10(){
    this.setState({
        begin:0,
        end:10,
        coba:10
    })
}
rows20(){
    this.setState({
        begin:0,
        end:20,
        coba:20
    })
}
rows30(){
    this.setState({
        begin:0,
        end:30,
        coba:30
    })
}
rows40(){
    this.setState({
        begin:0,
        end:40,
        coba:40
    })
}
rows50(){
    this.setState({
        begin:0,
        end:50,
        coba:50
    })
}
rows1(){
    this.setState({
        begin:0,
        end:1,
        coba:1
    })
}

render(){
    var filteredResource = this.state.resource.filter(
        (row) =>{
            return row.name.toLowerCase().indexOf(this.state.search.toLowerCase()) !== -1
        }
    ).slice(this.state.begin,this.state.end)
    return(<Container size="lg">
        <Detail 
        view = {this.state.currentList}
        detail = {this.state.viewList}
        closeModalhandler = {this.closeModalHandler}/>
        <EditList 
        view = {this.state.currentList}
        edit = {this.state.editList}
        closeModalhandler = {this.closeModalHandler}/>
         <TambahList 
        tambah = {this.state.tambahList}
        closeModalhandler = {this.closeModalHandler}/>
        <Hapus 
        coba = {this.state.currentList}
        hapus = {this.state.hapusList}
        closeModalhandler = {this.closeModalHandler}
        getList = {this.getListResourceAsc}/>
        <Row style={{borderBottomStyle:'ridge',borderBottomColor:'#000066'}}> 
        <Col xs="auto" style={{columnWidth:'100px'}}><Row style={{fontSize:'10px'}}>Client Name</Row><Row><Input type="Search" class="form-control" reqiured placeholder="Client" value={this.state.search} onChange={this.updateSearch.bind(this)}/></Row></Col>
        <Col xs="auto" style={{columnWidth:'100px'}}><Row style={{fontSize:'10px'}}>From</Row><Row><Input type="Date" class="form-control" reqiured placeholder="From" value={this.state.search1.start_project} name="start_project" onChange={this.changeHandler}/></Row><Row><Label for="text" style={{color:'#ff0000',fontSize:'14px',fontStyle:'italic'}}>{this.state.FromError}</Label></Row></Col>
        <Col xs="auto" style={{columnWidth:'100px'}}><Row style={{fontSize:'10px'}}>Until</Row><Row><Input type="Date" class="form-control" reqiured placeholder="After" value={this.state.search1.end_project} name="end_project" onChange={this.changeHandler} /></Row><Row><Label for="text" style={{color:'#ff0000',fontSize:'14px',fontStyle:'italic'}}>{this.state.UntilError}</Label></Row></Col>
        <Col xs="auto"  style={{columnWidth:'100px'}}><span className="fa fa-search" onClick={this.getListResourceFind}style={{fontSize : '25px',paddingRight:'30px',paddingTop:'20px'}}/></Col>
        <Col xs="auto" style={{columnWidth:'100px'}}>
        <ButtonDropdown isOpen={this.state.ascending} onClick={this.dropdownOpen} toggle={this.dropdownClose}>
      <DropdownToggle caret style={{backgroundColor:'#000066',color:'#ffffff'}}>AZ
      </DropdownToggle>
      <DropdownMenu>
        <DropdownItem header style={{fontSize:'12px',float:'left'}}>Order</DropdownItem>
        <DropdownItem onClick={this.getListResourceAsc}>Ascending</DropdownItem>
        <DropdownItem onClick={this.getListResourceDesc}>Descending</DropdownItem>
      </DropdownMenu></ButtonDropdown>
      <ButtonDropdown isOpen={this.state.pagenation} onClick={this.dropdownOpen1} toggle={this.dropdownClose1}>
      <DropdownToggle caret style={{backgroundColor:'#000066',color:'#ffffff'}}>=
      </DropdownToggle>
      <DropdownMenu>
        <DropdownItem header style={{fontSize:'12px',float:'left'}}>Row Per Page</DropdownItem>
        <DropdownItem onClick={this.rows1.bind(this)}>1</DropdownItem>
        <DropdownItem onClick={this.rows10.bind(this)}>10</DropdownItem>
        <DropdownItem onClick={this.rows20.bind(this)}>20</DropdownItem>
        <DropdownItem onClick={this.rows30.bind(this)}>30</DropdownItem>
        <DropdownItem onClick={this.rows40.bind(this)}>40</DropdownItem>
        <DropdownItem onClick={this.rows50.bind(this)}>50</DropdownItem>
      </DropdownMenu>
    </ButtonDropdown>
    <Button onClick={this.tambahModalHandler}style={{backgroundColor:'#000066',color:'#ffffff',borderRadius:'40px',fontSize:'20px'}}>+</Button></Col>
        </Row>
        <Row>
        <table class="table table-hover text-nowrap">
          <thead>
              <tr>
                  </tr></thead> 
                  <tbody>
                      <td>Client</td>
                      <td>Start Date</td>
                      <td>End Date</td>
                      <td>#</td>
                      {
                         filteredResource.map((row)=>
                      <tr><td>{row.name}</td>
                          <td>{row.start_project}</td>
                      <td>{row.end_project}</td>
                      <td>
                                        <Link to='#'>
     
                <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" toggle="menu-close" data-accordion="false" style={{fonSize:'14px',backgroundColor:'#ffffff'}}>

                  <li class="nav-item has-treeview menu-close"  >
                    <a href="#" class="nav-link" style={{fonSize:'14px',backgroundColor:'#000066',color:'#ffffff'}}>
                      <p>
                        More
                        <i class="right fas fa-angle-left"></i>
                      </p>
                    </a>
                    <ul class="nav nav-treeview" >
                      <li class="nav-item">
                        <a onClick={()=>{this.viewModalHandler(row.id)}}class="nav-link">
                          <p>Detail</p>
                        </a>
                      </li>
                    </ul>
                    <ul class="nav nav-treeview">
                      <li class="nav-item">
                        <a onClick={()=>{this.editModalHandler(row.id)}}class="nav-link">
                          <p>Ubah</p>
                        </a>
                      </li>
                    </ul>
                     <ul class="nav nav-treeview">
                      <li class="nav-item">
                        <a onClick={()=>{this.hapusModalHandler(row.id)}}class="nav-link">
                          <p>Hapus</p>
                        </a>
                      </li>
                    </ul>
                  </li>
   
                </ul>

                                        </Link></td>
                         </tr>)
                      }
                      
                      </tbody>
         </table></Row>
         <button class="float-right" onClick={this.next}>Next</button>
         <button class="float-right" onClick={this.previous.bind(this)}>  Previous </button>
         <Pagination
                    bsSize="medium"
                    items={1}
                    activePage={1}/>
         </Container>
    )
}
}export default list