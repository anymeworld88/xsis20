import React from 'react'
import {Modal,ModalBody,ModalFooter,ModalHeader,Button,Container,Collapse,Row,Col,Label,Input,Form,FormGroup,ButtonDropdown, DropdownToggle, DropdownMenu, DropdownItem} from 'reactstrap'
import axios from 'axios'
import apiconfig from '../../../configs/api.configs.json'

class Detail extends React.Component{
    constructor(props){
        super(props)
        this.state={
            information:false,PiCol:false,PD:false,PT:false,MT:false
        }
    this.dropdownInformationO = this.dropdownInformationO.bind(this)
    this.dropdownInformationC = this.dropdownInformationC.bind(this)
}
PICol(){
this.setState({
  PiCol:!this.state.PiCol,
  PD:false,
  PT:false,
  MT:false
})
}
PDonClick(){
  this.setState({
    PD:!this.state.PD,
    PiCol:false,
    PT:false,
    MT:false
  })
  }
  PTonClick(){
    this.setState({
      PT:!this.state.PT,
      PiCol:false,
      PD:false,
      MT:false
    })
    }
    MTonClick(){
      this.setState({
        MT:!this.state.MT,
        PiCol:false,
        PD:false,
        PT:false
      })
      }

dropdownInformationO(){
    this.setState({
        information:true
    })
}
dropdownInformationC(){
    this.setState({
        information:false
    })
}

    render(){
        return(
            <Modal style={{color:'#000066'}}isOpen={this.props.detail} className={this.props.className} size='lg'>
                  <ModalHeader style={{color:'#ffffff', backgroundColor:'#000066'}}><Container><Row><Col size="lg" xs="6" style={{columnWidth:'700px'}}>Detail</Col><Col size="sm"><Button style={{color:'#ffffff',float:'right'}}close onClick={this.props.closeModalhandler} class="rounded float-right"/></Col></Row></Container></ModalHeader>
                  <ModalBody>
                   
                  <ul class="nav nav-pills nav-sidebar flex-column"  role="menu" data-toggle="collapse" size="lg"style={{fonSize:'14px',backgroundColor:'#ffffff',color:'#000066'}}>
                  <li class="nav-item menu-close">
                    <a href="#" class="nav-link " onClick={this.PICol.bind(this)} onC style={{borderStyle:'ridge'}}>
                      <p style={{color:'#000066'}}>
                        Project Information
                        <i class="right fas fa-angle-left"></i>
                      </p>
                    </a>
                    <Collapse isOpen={this.state.PiCol} hide style={{fonSize:'14px',backgroundColor:'#ffffff',color:'#000066'}}>
                         <Row><Col width="200px">Client *</Col><Col width="200px">Location</Col></Row>
                         <Row style={{fontWeight:'Bold',fontSize:'14px'}}><Col width="200px">{this.props.view.name}</Col><Col width="200px">{this.props.view.location}</Col></Row>
                         <Row><Col width="200px">Department</Col><Col width="200px">User/PIC Name</Col></Row>
                         <Row style={{fontWeight:'Bold',fontSize:'14px'}}><Col width="200px">{this.props.view.department}</Col><Col width="200px">{this.props.view.pic_name}</Col></Row>
                         <Row><Col width="200px">Project Name</Col><Col><Row><Col style={{width:'100px'}}>Start Date</Col><Col style={{width:'100px'}}>End Date</Col></Row></Col></Row>
                         <Row style={{fontWeight:'Bold',fontSize:'14px'}}><Col width="200px">{this.props.view.project_name}</Col><Col><Row><Col style={{width:'100px'}}>{this.props.view.start_project}</Col><Col style={{width:'100px'}}>{this.props.view.end_project}</Col></Row></Col></Row>
                         <Row><Col width="200px">Project Role</Col><Col width="200px">Project Phase</Col></Row>
                         <Row style={{fontWeight:'Bold',fontSize:'14px'}}><Col width="200px">{this.props.view.project_role}</Col><Col width="200px">{this.props.view.project_phase}</Col></Row>
    
                    </Collapse>
                  </li>
                  <li class="nav-item menu-close" >
                    <a href="#" class="nav-link" onClick={this.PDonClick.bind(this)}style={{borderStyle:'ridge'}}>
                      <p style={{color:'#000066'}}>
                        Project Description
                        <i class="right fas fa-angle-left"></i>
                      </p>
                    </a>
                    <Collapse isOpen={this.state.PD}  style={{fonSize:'14px',backgroundColor:'#ffffff',color:'#000066'}}>
                      
                      
                         <Row><Col width="200px">Project Description</Col></Row>
                         <Row style={{fontWeight:'Bold',fontSize:'14px'}}><Col width="200px">{this.props.view.project_description}</Col></Row>
                   
                      
                    </Collapse>
                  </li>
                  <li class="nav-item menu-close toggle-menu"  >
                    <a href="#" class="nav-link" onClick={this.PTonClick.bind(this)} style={{borderStyle:'ridge'}}>
                      <p style={{color:'#000066'}}>
                        Project Technology
                        <i class="right fas fa-angle-left"></i>
                      </p>
                    </a>
                    <Collapse isOpen={this.state.PT}  style={{fonSize:'14px',backgroundColor:'#ffffff',color:'#000066'}}>
                      
                     
                         <Row><Col width="200px">Project Technology</Col></Row>
                         <Row style={{fontWeight:'Bold',fontSize:'14px'}}><Col width="200px">{this.props.view.project_technology}</Col></Row>
                
                      
                    </Collapse>
                  </li>
                  <li class="nav-item menu-close" >
                    <a href="#" class="nav-link" onClick={this.MTonClick.bind(this)} style={{borderStyle:'ridge'}}>
                      <p style={{color:'#000066'}}>
                        Main Task
                        <i class="right fas fa-angle-left"></i>
                      </p>
                    </a>
                    <Collapse isOpen={this.state.MT} style={{fonSize:'14px',backgroundColor:'#ffffff',color:'#000066'}}>
                      
                     
                         <Row><Col width="200px">Main Task</Col></Row>
                         <Row style={{fontWeight:'Bold',fontSize:'14px'}}><Col width="200px">{this.props.view.main_task}</Col></Row>
                 
                      
                    </Collapse>
                  </li>
                </ul>
                  </ModalBody>
                  <ModalFooter>
                      <Button color="warning"onClick={this.props.closeModalhandler}>Close</Button>
                  </ModalFooter>
            </Modal>
        )
    }
}export default Detail