import React from 'react'
import {Modal,ModalBody,ModalFooter,ModalHeader,Button,Container,Row,Col,Label,Input,Form,FormGroup} from 'reactstrap'
import axios from 'axios'
import apiconfig from '../../../configs/api.configs.json'

class TambahList extends React.Component{
    constructor (props){
        super(props)
        let userdata = JSON.parse(localStorage.getItem(apiconfig.LS.USERDATA))
        this.state={
            formdata:{
                client_id:'',
                location:'',
                department:'',
                pic_name:'',
                project_name:'',
                start_project:'',
                end_project:'',
                project_role:'',
                project_phase:'',
                project_description:'',
                project_technology:'',
                main_task:''
            },
            id:'1',Client:[],
            CError:'',LError:'',DError:'',PICError:'',PNError:'',STARTError:'',ENDError:'',PRError:'',PPError:'',PDError:'',PTECHError:'',MAINError:'',TanggalError:''
        }
        this.changeHandler = this.changeHandler.bind(this)
        this.submitHandler = this.submitHandler.bind(this)
        this.close = this.close.bind(this)
    }
    changeHandler(e){
        let tmp = this.state.formdata
        tmp[e.target.name]=e.target.value
        this.setState({
            formdata:tmp
        })
    }
    Tanggal(){
        var a = new Date().getFullYear() ;
        var aa = new Date().getMonth()+1 ;
        var aaa = new Date().getDate();
        var b = new Date().getHours();
        var bb = new Date().getMinutes();
        var bbb = new Date().getSeconds();
        var c = a+'-'+aa+'-'+aaa+" "+b+":"+bb+":"+bbb
        this.setState({
            Tanggal:c
        })
    }
    componentDidMount(){
        this.Tanggal()
        this.getListClient()
    }
    getListClient(){
        let token = localStorage.getItem(apiconfig.LS.TOKEN)
      let option = {
          url: apiconfig.BASE_URL+apiconfig.ENDPOINTS.CLIENT,
          method:"get",
          header:{
              "Authorization": token
          }
      }
      axios(option).then((response)=>{
          if(response.data.code ==200){
            let tmp = response.data.message
            this.setState({
              Client: tmp     
            })
          }else{
              alert(response.data.message)
          }
      }).catch((error)=>{
          alert(error)
      })
    }
    validate(){
        let isError=false
        var a = this.state.formdata.start_project ;
        var b = this.state.formdata.end_project
        var c = b.split('-')
        var d = a.split('-')
        var f = d[0] - c[0]
        var ff = d[1] - c[1]
        var fff = d[2] - c[2]
        const errors={ CError:'',LError:'',DError:'',PICError:'',PNError:'',STARTError:'',ENDError:'',PRError:'',PPError:'',PDError:'',PTECHError:'',MAINError:'',TanggalError:''}
        if (fff >=0 ){
            if(ff>=0){
                if(f>=0){
            isError= true
            errors.TanggalError = 'Tanggal Melebihi Start Project'}}
            else if(ff<=0){
                if(f>=0){
            isError= true
            errors.TanggalError = 'Tanggal Melebihi Start Project'}}
        }
        else if (fff < 0 ){
            if(ff>0){
                if(f>=0){
            isError= true
            errors.TanggalError = 'Tanggal Melebihi Start Project'}}
            else if(ff<0){
                if(f>0){
            isError= true
            errors.TanggalError = 'Tanggal Melebihi Start Project'}}
        }
    
        if (this.state.formdata.client_id == ""  ){
            isError= true
            errors.CError = 'Harus Di Isi'
        }
        if (this.state.formdata.project_name == ""  ){
            isError= true
            errors.PNError = 'Harus Di Isi'
        }
        if (this.state.formdata.start_project == ""  ){
            isError= true
            errors.STARTError = 'Harus Di Isi'
        }
        if (this.state.formdata.end_project == ""  ){
            isError= true
            errors.ENDError = 'Harus Di Isi'
        }
        if (this.state.formdata.project_role == ""  ){
            isError= true
            errors.PRError = 'Harus Di Isi'
        }
        if (this.state.formdata.project_phase == ""  ){
            isError= true
            errors.PPError = 'Harus Di Isi'
        }
        if (this.state.formdata.project_description == ""  ){
            isError= true
            errors.PDError = 'Harus Di Isi'
        }
        if (this.state.formdata.project_technology == ""  ){
            isError= true
            errors.PTECHError = 'Harus Di Isi'
        }
        if (this.state.formdata.main_task == ""  ){
            isError= true
            errors.MAINError = 'Harus Di Isi'
        }
        if(isError){
            this.setState({
                ...this.state.formdata,
                ...errors
            })
        }
        return isError
    }
    close(){
        this.setState({
            formdata:{},CError:'',LError:'',DError:'',PICError:'',PNError:'',STARTError:'',ENDError:'',PRError:'',PPError:'',PDError:'',PTECHError:'',MAINError:'',TanggalError:''
        })
        this.props.closeModalhandler()
    }
    submitHandler(e){
        e.preventDefault()
        const err = this.validate()
        if(!err){
            this.state.formdata.created_on=this.state.Tanggal
            this.state.formdata.created_by=this.state.id
        let token = localStorage.getItem(apiconfig.LS.TOKEN)
        let option = {
            url: apiconfig.BASE_URL+apiconfig.ENDPOINTS.TAMBAHCLIENT,
            method :"post",
            headers:{
                "Authorization":token,
                "Content-Type" : "application/json"
            },
            data: this.state.formdata
        }



        axios(option).then((response)=>{
            if(response.data.code ==200){
                alert('Success')
                this.props.history.push('/dashboard')

            }else{
                alert(response.data.message)
            }
        }).catch((error)=>{
            console.log(error)
        })
        this.setState({
            CError:'',LError:'',DError:'',PICError:'',PNError:'',STARTError:'',ENDError:'',PRError:'',PPError:'',PDError:'',PTECHError:'',MAINError:'',TanggalError:''
        })
        this.props.closeModalhandler()
    }

}
    render(){
        return(
            <Modal isOpen={this.props.tambah} className={this.props.className} size="lg">
                
                <ModalHeader style={{color:'#ffffff', backgroundColor:'#000066'}}>Resource Project Form</ModalHeader>
                <ModalBody>
                    <Row style={{fontStyle:'italic',fontSize:'10px'}}><Col width="200px">Client *</Col><Col width="200px">Location</Col></Row>
                         <Row ><Col width="200px">
                         <select 
                      className="form-control" 
                      name="client_id" 
                      onChange={this.changeHandler}
                      value={this.state.formdata.client_id}>
                         <option value=''>Pilih</option>
                         {this.state.Client.map((row,x)=>
                                <option value={row.id}>{row.name}</option>)
                            }   
                 </select>
                 <Label for="text" style={{color:'#ff0000',fontSize:'14px',fontStyle:'italic'}}>{this.state.CError}</Label></Col><Col width="200px">
                        <Input type="text" class="form-control"
                    name="location"
                    value={this.state.formdata.location}
                    onChange={this.changeHandler}
                    reqiured placeholder="Location"/>
                   </Col></Row>
                         <Row  style={{fontStyle:'italic',fontSize:'10px'}}><Col width="200px">Department</Col><Col width="200px">User/PIC Name </Col></Row>
                         <Row ><Col width="200px">
                         <Input type="text" class="form-control"
                    name="department"
                    value={this.state.formdata.department}
                    onChange={this.changeHandler}
                    reqiured placeholder="Department"/>
                     </Col><Col width="200px">
                         <Input type="text" class="form-control"
                    name="pic_name"
                    value={this.state.formdata.pic_name}
                    onChange={this.changeHandler}
                    reqiured placeholder="User"/>
                    </Col></Row>
                         <Row style={{fontStyle:'italic',fontSize:'10px'}}><Col  width="200px">Project Name *</Col><Col><Row><Col style={{width:'100px'}}>Start Date *</Col><Col style={{width:'100px'}}>End Date *</Col></Row></Col></Row>
                         <Row ><Col width="200px"> 
                         <Input type="text" class="form-control"
                    name="project_name"
                    value={this.state.formdata.project_name}
                    onChange={this.changeHandler}
                    reqiured placeholder="Project Name"/>
                    <Label for="text" style={{color:'#ff0000',fontSize:'14px',fontStyle:'italic'}}>{this.state.PNError}</Label></Col><Col><Row><Col style={{width:'100px'}}> 
                    <Input type="date" class="form-control"
                    name="start_project"
                    value={this.state.formdata.start_project}
                    onChange={this.changeHandler}
                   />
                   <Label for="text" style={{color:'#ff0000',fontSize:'14px',fontStyle:'italic'}}>{this.state.STARTError}</Label></Col><Col style={{width:'100px'}}>
                         <Input type="date" class="form-control"
                    name="end_project"
                    value={this.state.formdata.end_project}
                    onChange={this.changeHandler}
                    />
                    <Label for="text" style={{color:'#ff0000',fontSize:'14px',fontStyle:'italic'}}>{this.state.ENDError}</Label>
                    <Label for="text" style={{color:'#ff0000',fontSize:'14px',fontStyle:'italic'}}>{this.state.TanggalError}</Label></Col></Row></Col></Row>
                         <Row  style={{fontStyle:'italic',fontSize:'10px'}}><Col width="200px">Project Role *</Col><Col width="200px">Project Phase *</Col></Row>
                         <Row ><Col width="200px">
                              <Input type="text" class="form-control"
                    name="project_role"
                    value={this.state.formdata.project_role}
                    onChange={this.changeHandler}
                    reqiured placeholder="Project Role"/>
                    <Label for="text" style={{color:'#ff0000',fontSize:'14px',fontStyle:'italic'}}>{this.state.PRError}</Label></Col><Col width="200px">
                         <Input type="text" class="form-control"
                    name="project_phase"
                    value={this.state.formdata.project_phase}
                    onChange={this.changeHandler}
                    reqiured placeholder="Project Phase"/>
                    <Label for="text" style={{color:'#ff0000',fontSize:'14px',fontStyle:'italic'}}>{this.state.PPError}</Label></Col></Row>
                     <Row style={{fontStyle:'italic',fontSize:'10px'}}><Col width="200px">Project Description *</Col></Row>
                         <Row><Col width="500px">
                         <Input type="textarea" class="form-control"
                    name="project_description"
                    value={this.state.formdata.project_description}
                    onChange={this.changeHandler}
                    reqiured placeholder="Project Description"/>
                    <Label for="text" style={{color:'#ff0000',fontSize:'14px',fontStyle:'italic'}}>{this.state.PDError}</Label></Col></Row>
                    <Row style={{fontStyle:'italic',fontSize:'10px'}}><Col width="200px">Project Technology *</Col></Row>
                         <Row><Col width="500px">
                         <Input type="textarea" class="form-control"
                    name="project_technology"
                    value={this.state.formdata.project_technology}
                    onChange={this.changeHandler}
                    reqiured placeholder="Project Technology"/>
                    <Label for="text" style={{color:'#ff0000',fontSize:'14px',fontStyle:'italic'}}>{this.state.PTECHError}</Label></Col></Row>
                    <Row style={{fontStyle:'italic',fontSize:'10px'}}><Col width="200px">Main Task *</Col></Row>
                         <Row><Col width="500px">
                         <Input type="textarea" class="form-control"
                    name="main_task"
                    value={this.state.formdata.main_task}
                    onChange={this.changeHandler}
                    reqiured placeholder="Main Task"/>
                    <Label for="text" style={{color:'#ff0000',fontSize:'14px',fontStyle:'italic'}}>{this.state.MAINError}</Label></Col></Row>
                </ModalBody>
                <ModalFooter>
                    <Button color="warning"onClick={this.close}>Close</Button>
                    <Button color="primary"onClick={this.submitHandler}>Save</Button></ModalFooter>
            </Modal>
        )
    }
} export default TambahList