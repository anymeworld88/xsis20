import React from 'react'
import apiconfig from '../../../configs/api.configs.json'
import {Modal,ModalBody,ModalFooter,ModalHeader,Button,Container,Row,Col,Label,Input,Form,FormGroup,ButtonDropdown, DropdownToggle, DropdownMenu, DropdownItem} from 'reactstrap'
import axios from 'axios'
import {Link} from 'react-router-dom'

class Hapus extends React.Component{
    constructor (props){
        super(props)
        let userdata = JSON.parse(localStorage.getItem(apiconfig.LS.USERDATA))

        this.state = {
            formdata:{
                deleted_on:'',
                is_delete:'',
                created_by:'',
                id:''
            },
           Tanggal:'',
           id:'1',

        }
        this.submitHandler=this.submitHandler.bind(this)
    }
    componentWillReceiveProps(newProps){
        this.setState({
            formdata:newProps.coba
        })


    }
    submitHandler(){

         this.state.formdata.deleted_on=this.state.Tanggal
         this.state.formdata.is_delete=true
         this.state.formdata.deleted_by=this.state.id
        let token = localStorage.getItem(apiconfig.LS.TOKEN)
        let option = {
            url: apiconfig.BASE_URL+apiconfig.ENDPOINTS.HAPUSLIST,
            method :"put",
            headers:{
                "Authorization":token,
                "Content-Type" : "application/json"
            },
            data: this.state.formdata
        }

        axios(option).then((response)=>{
            if(response.data.code ==200){
                alert('Success')
                this.props.history.push('/dashboard')

            }else{
                alert(response.data.message)
            }
        }).catch((error)=>{
            console.log(error)
        })

        this.props.closeModalhandler()
        this.props.getList()
        
}   
    
Tanggal(){
        var a = new Date().getFullYear() ;
        var aa = new Date().getMonth()+1 ;
        var aaa = new Date().getDate();
        var b = new Date().getHours();
        var bb = new Date().getMinutes();
        var bbb = new Date().getSeconds();
        var c = a+'-'+aa+'-'+aaa+" "+b+":"+bb+":"+bbb
        this.setState({
            Tanggal:c
        })
    }
    componentDidMount(){
        this.Tanggal()
    }
    render(){
        return(
            <Modal style={{color:'#ffffff',backgroundColor:'#ff0000'}}isOpen={this.props.hapus} className={this.props.className}>
                  <ModalHeader style={{color:'#ffffff',backgroundColor:'#ff0000'}} >Hapus ?</ModalHeader>
                  <ModalBody><Container><Row><Col xs="auto">
        <span  xs="auto" class="fa fa-trash" style={{fontSize : '50px',color:'#ff0000'}}/></Col><Col xs="auto" ><Row><Col xs="auto" style={{color:'#ff0000'}} > Apa anda yakin Ingin menghapus project di</Col></Row><Row><Col xs="auto"  style={{fontWeight:'Bold',color:'#ff0000'}}>{this.state.formdata.name}</Col></Row> </Col></Row></Container>
                  </ModalBody>
                  <ModalFooter>
                      <Button color="warning"onClick={this.props.closeModalhandler}>Tidak</Button>
                      <Button style={{color:'#ffffff',backgroundColor:'#ff0000'}} onClick={this.submitHandler}>Ya</Button>
                  </ModalFooter>
            </Modal>
        )
    }
} export default Hapus