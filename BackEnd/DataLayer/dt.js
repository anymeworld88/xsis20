const pg = require('pg')
const DatabaseConnection = require('../Config/dbp.config.json')
var DB = new pg.Pool(DatabaseConnection.config)
const bcrypt = require('bcryptjs')
const dt ={
    readBiodataAllHandlerData: (callback)=> {
        DB.connect(function(err,client,done){
            var data = ''
            if(err){
                data = err
            }
            client.query('select  x_biodata.id,fullname,nick_name,pob,dob,gender,religion_id,high,weight,nationality,ethnic,hobby,identity_type_id,identity_no,email,phone_number1,phone_number2,parent_phone_number,child_sequence,how_many_brothers,marital_status_id,marriage_year,x_religion.name as agama,x_identity_type.name as identitas,x_marital_status.name as statuskuh,file_path,file_name from x_biodata inner join x_religion on x_biodata.religion_id = x_religion.id inner join x_identity_type on x_biodata.identity_type_id = x_identity_type.id inner join x_marital_status on x_biodata.marital_status_id = x_marital_status.id inner join x_biodata_attachment on x_biodata.id =  x_biodata_attachment.biodata_id where x_biodata.is_delete=false and x_biodata_attachment.is_photo=true',function(err,result){
                done()
                if(err){
                    data=err;
                }else{
                    for (i=0;i<result.rows.length;i++){
                        const options = { year: 'numeric', month: '2-digit', day: '2-digit' }
                        let exptoken = (result.rows[i].dob).toLocaleDateString(undefined, options)

                        result.rows[i].dob = exptoken

                    }
                    data=result.rows
                }
                callback(data)
            })
        })
    },
    readView2AllHandlerData: (callback,docs)=> {
        DB.connect(function(err,client,done){
            var data =''
            let adrs=''
            if(err){
                data=err
            }
            const query ={
                text:'select  x_biodata.id,fullname,nick_name,pob,dob,gender,religion_id,high,weight,nationality,ethnic,hobby,identity_type_id,identity_no,email,phone_number1,phone_number2,parent_phone_number,child_sequence,how_many_brothers,marital_status_id,marriage_year,x_religion.name as agama,x_identity_type.name as identitas,x_marital_status.name as statuskuh from x_biodata inner join x_religion on x_biodata.religion_id = x_religion.id inner join x_identity_type on x_biodata.identity_type_id = x_identity_type.id inner join x_marital_status on x_biodata.marital_status_id = x_marital_status.id where biodata_id=($1) and x_biodata.is_delete=false',
                values:[docs.id]
            }
            client.query(query,function(err,result){
                done()
                if(err){
                    data=err
                }
                else{ data=result.rows}
               
                callback(data)
            })
        })
    },
    updateBiodataAllHandlerData: (callback,docs)=>{
        DB.connect(function(err,client,done){
            var data =''
            let adrs=''
            if(err){
                data=err
            }
            const query ={
                text:'update x_biodata set modified_on=($23),fullname=($1),nick_name=($2),pob=($3),dob=($4),gender=($5),religion_id=($6),high=($7),weight=($8),nationality=($9),ethnic=($10),hobby=($11),identity_type_id=($12),identity_no=($13),child_sequence=($14),how_many_brothers=($15),marital_status_id=($16),marriage_year=($17),email=($19),phone_number1=($20),phone_number2=($21),parent_phone_number=($22) where id=($18)',
                values:[docs.fullname,docs.nick_name,docs.pob,docs.dob,docs.gender,docs.religion_id,docs.high,docs.weight,docs.nationality,docs.ethnic,docs.hobby,docs.identity_type_id,docs.identity_no,docs.child_sequence,docs.how_many_brothers,docs.marital_status_id,docs.marriage_year,docs.id,docs.email,docs.phone_number1,docs.phone_number2,docs.parent_phone_number,docs.modified_on]
            }
            client.query(query,function(err,result){
                done()
                if(err){
                    data=err
                }
                else{ data=[data=docs,
                    message="Data Berhasil Di Update"]}
               
                callback(data)
            })
        })
    },updateAlamatAllHandlerData: (callback,docs)=>{
        DB.connect(function(err,client,done){
            var data =''
            let adrs=''
            if(err){
                data=err
            }
            const query ={
                text:'update x_address set modified_on=($16),address1=($1),postal_code1=($2),rt1=($3),rw1=($4),kelurahan1=($5),kecamatan1=($6),region1=($7),address2=($8),postal_code2=($9),rt2=($10),rw2=($11),kelurahan2=($12),kecamatan2=($13),region2=($14) where biodata_id=($15)',
                values:[docs.address1,docs.postal_code1,docs.rt1,docs.rw1,docs.kelurahan1,docs.kecamatan1,docs.region1,docs.address2,docs.postal_code2,docs.rt2,docs.rw2,docs.kelurahan2,docs.kecamatan2,docs.region2,docs.biodata_id,docs.modified_on]
            }
            client.query(query,function(err,result){
                done()
                if(err){
                    data=err
                }
                else{ data=[data=docs,
                    message="Data Berhasil Di Update"]}
               
                callback(data)
            })
        })
    },
    readBiodataAllHandlerData1: (callback,docs)=> {
        DB.connect(function(err,client,done){
            var data = ''
            if(err){
                data = err
            }
            const query ={
                text:'select x_biodata.modified_on,x_biodata.id,fullname,nick_name,pob,dob,gender,religion_id,high,weight,nationality,ethnic,hobby,identity_type_id,identity_no,email,phone_number1,phone_number2,parent_phone_number,child_sequence,how_many_brothers,marital_status_id,marriage_year,biodata_id,address1,postal_code1,rt1,rw1,kelurahan1,kecamatan1,region1,address2,postal_code2,rt2,rw2,kelurahan2,kecamatan2,region2,x_religion.name from x_biodata inner join x_address on x_biodata.id = x_address.biodata_id inner join x_religion on x_biodata.religion_id = x_religion.id where x_biodata.id=($1)',
                values:[docs.id]
            }
            client.query(query,function(err,result){
                done()
                if(err){
                    data=err;
                }else{
                    for (i=0;i<result.rows.length;i++){
                        const options = { year: 'numeric', month: '2-digit', day: '2-digit' }
                        let exptoken = (result.rows[i].dob).toLocaleDateString(undefined, options)

                        result.rows[i].dob = exptoken
                    }
                    data=result.rows
                }
                callback(data)
            })
        })
    },
    readPendidikanAllHandlerData: (callback,docs)=> {
        DB.connect(function(err,client,done){
            var data =''
            let adrs=''
            if(err){
                data=err
            }
            const query ={
                text:'select * from x_riwayat_pendidikan where biodata_id=($1) order by graduation_year desc',
                values:[docs.id]
            }
            client.query(query,function(err,result){
                done()
                if(err){
                    data=err
                }
                else{ data=result.rows}
               
                callback(data)
            })
        })
    },
    readPendidikan1AllHandlerData: (callback,docs)=> {
        DB.connect(function(err,client,done){
            var data =''
            let adrs=''
            if(err){
                data=err
            }
            const query ={
                text:'select * from x_riwayat_pendidikan where biodata_id=($1) order by graduation_year desc limit 1',
                values:[docs.id]
            }
            client.query(query,function(err,result){
                done()
                if(err){
                    data=err
                }
                else{ data=result.rows}
               
                callback(data)
            })
        })
    },
    readPekerjaanAllHandlerData: (callback,docs)=> {
        DB.connect(function(err,client,done){
            var data =''
            let adrs=''
            if(err){
                data=err
            }
            const query ={
                text:'select * from x_riwayat_pekerjaan where biodata_id=($1) order by resign_year desc',
                values:[docs.id]
            }
            client.query(query,function(err,result){
                done()
                if(err){
                    data=err
                }
                else{ data=result.rows}
               
                callback(data)
            })
        })
    },
    readKeahlianAllHandlerData: (callback,docs)=> {
        DB.connect(function(err,client,done){
            var data =''
            let adrs=''
            if(err){
                data=err
            }
            const query ={
                text:"SELECT STRING_AGG (skill_name, ',') as skill_name,x_education_level.*FROM x_keahlian inner join x_education_level on x_keahlian.skill_level_id = x_education_level.id  where x_keahlian.biodata_id =($1)  group by x_education_level.id,skill_level_id",
                values:[docs.id]
            }
            client.query(query,function(err,result){
                done()
                if(err){
                    data=err
                }
                else{ data=result.rows}
               
                callback(data)
            })
        })
    },
    readTrainingAllHandlerData: (callback,docs)=> {
        DB.connect(function(err,client,done){
            var data =''
            let adrs=''
            if(err){
                data=err
            }
            const query ={
                text:'select * from x_riwayat_pelatihan inner join x_time_period on x_riwayat_pelatihan.time_period_id = x_time_period.id where biodata_id=($1) order by training_year desc',
                values:[docs.id]
            }
            client.query(query,function(err,result){
                done()
                if(err){
                    data=err
                }
                else{ data=result.rows}
               
                callback(data)
            })
        })
    },
    readSertifikasiAllHandlerData: (callback,docs)=> {
        DB.connect(function(err,client,done){
            var data =''
            let adrs=''
            if(err){
                data=err
            }
            const query ={
                text:'select * from x_sertifikasi where biodata_id=($1) order by until_year desc',
                values:[docs.id]
            }
            client.query(query,function(err,result){
                done()
                if(err){
                    data=err
                }
                else{ data=result.rows}
                callback(data)
            })
        })
    },
    readReligionAllHandlerData: (callback)=> {
        DB.connect(function(err,client,done){
            var data =''
            let adrs=''
            if(err){
                data=err
            }
            
            client.query('select id,name from x_religion',function(err,result){
                done()
                if(err){
                    data=err
                }
                else{ data=result.rows}
                callback(data)
            })
        })
    },
    readIdentityAllHandlerData: (callback)=> {
        DB.connect(function(err,client,done){
            var data =''
            let adrs=''
            if(err){
                data=err
            }
            
            client.query('select id,name from x_identity_type',function(err,result){
                done()
                if(err){
                    data=err
                }
                else{ data=result.rows}
                callback(data)
            })
        })
    },
    readMaritalAllHandlerData: (callback)=> {
        DB.connect(function(err,client,done){
            var data =''
            let adrs=''
            if(err){
                data=err
            }
            
            client.query('select id,name from x_marital_status',function(err,result){
                done()
                if(err){
                    data=err
                }
                else{ data=result.rows}
                callback(data)
            })
        })
    },
    readEmailAllHandlerData: (callback,docs)=> {
        DB.connect(function(err,client,done){
            var data =''
            let adrs=''
            if(err){
                data=err
            }
            const query ={
                text:'select email from x_biodata where id!=($1)',
                values:[docs.id]
            }
            client.query(query,function(err,result){
                done()
                if(err){
                    data=err
                }
                else{ data=result.rows}
                callback(data)
            })
        })
    },
    readNoHp1AllHandlerData: (callback,docs)=> {
        DB.connect(function(err,client,done){
            var data =''
            let adrs=''
            if(err){
                data=err
            }
            const query ={
                text:'select phone_number1 from x_biodata where id!=($1)',
                values:[docs.id]
            }
            client.query(query,function(err,result){
                done()
                if(err){
                    data=err
                }
                else{ data=result.rows}
                callback(data)
            })
        })
    },
    readNoIdentity1AllHandlerData: (callback,docs)=> {
        DB.connect(function(err,client,done){
            var data =''
            let adrs=''
            if(err){
                data=err
            }
            const query ={
                text:'select identity_no from x_biodata where id!=($1)',
                values:[docs.id]
            }
            client.query(query,function(err,result){
                done()
                if(err){
                    data=err
                }
                else{ data=result.rows}
                callback(data)
            })
        })
    },
    readPhotoProfileAllHandlerData: (callback,docs)=> {
        DB.connect(function(err,client,done){
            var data =''
            let adrs=''
            if(err){
                data=err
            }
            const query ={
                text:'select file_name,file_path from  x_biodata_attachment where biodata_id=($1) and is_photo=true',
                values:[docs.id]
            }
            client.query(query,function(err,result){
                done()
                if(err){
                    data=err
                }
                else{ data=result.rows}
                callback(data)
            })
        })
    },
    readResourceAllHandlerData: (callback)=> {
        DB.connect(function(err,client,done){
            var data = ''
            if(err){
                data = err
            }
            client.query('select x_resource_project.*,name from x_resource_project inner join x_client on x_resource_project.client_id = x_client.id where x_resource_project.is_delete=false',function(err,result){
                done()
                if(err){
                    data=err;
                }else{
                    for (i=0;i<result.rows.length;i++){
                        const options = {  month: '2-digit', day: '2-digit' ,year: 'numeric'}
                        let exptoken = (result.rows[i].start_project).toLocaleDateString(undefined, options)

                        result.rows[i].start_project = exptoken
                    }
                    for (j=0;j<result.rows.length;j++){
                        const options = {  month: '2-digit', day: '2-digit',year: 'numeric' }
                        let exptoken = (result.rows[j].end_project).toLocaleDateString(undefined, options)

                        result.rows[j].end_project = exptoken
                    }
                    data=result.rows
                }
                callback(data)
            })
        })
    },
    readResourceDescAllHandlerData: (callback)=> {
        DB.connect(function(err,client,done){
            var data = ''
            if(err){
                data = err
            }
            client.query('select x_resource_project.*,name from x_resource_project inner join x_client on x_resource_project.client_id = x_client.id where x_resource_project.is_delete=false order by name desc',function(err,result){
                done()
                if(err){
                    data=err;
                }else{
                    for (i=0;i<result.rows.length;i++){
                        const options = {  month: '2-digit', day: '2-digit' ,year: 'numeric'}
                        let exptoken = (result.rows[i].start_project).toLocaleDateString(undefined, options)

                        result.rows[i].start_project = exptoken
                    }
                    for (j=0;j<result.rows.length;j++){
                        const options = {  month: '2-digit', day: '2-digit',year: 'numeric' }
                        let exptoken = (result.rows[j].end_project).toLocaleDateString(undefined, options)

                        result.rows[j].end_project = exptoken
                    }
                    data=result.rows
                }
                callback(data)
            })
        }) 
    },
    readResourceAscAllHandlerData: (callback)=> {
        DB.connect(function(err,client,done){
            var data = ''
            if(err){
                data = err
            }
            client.query('select x_resource_project.*,name from x_resource_project inner join x_client on x_resource_project.client_id = x_client.id where x_resource_project.is_delete=false order by name asc',function(err,result){
                done()
                if(err){
                    data=err;
                }else{
                    for (i=0;i<result.rows.length;i++){
                        const options = {  month: '2-digit', day: '2-digit' ,year: 'numeric'}
                        let exptoken = (result.rows[i].start_project).toLocaleDateString(undefined, options)

                        result.rows[i].start_project = exptoken
                    }
                    for (j=0;j<result.rows.length;j++){
                        const options = {  month: '2-digit', day: '2-digit',year: 'numeric' }
                        let exptoken = (result.rows[j].end_project).toLocaleDateString(undefined, options)

                        result.rows[j].end_project = exptoken
                    }
                    data=result.rows
                }
                callback(data)
            })
        })
    },
    hapusListAllHandlerData: (callback,docs)=>{
        DB.connect(function(err,client,done){
            var data =''
            let adrs=''
            if(err){
                data=err
            }
            const query ={
                text:'update x_resource_project set deleted_by=($1),deleted_on=($2),is_delete=($3) where id=($4)',
                values:[docs.deleted_by,docs.deleted_on,docs.is_delete,docs.id]
            }
            client.query(query,function(err,result){
                done()
                if(err){
                    data=err
                }
                else{ data=[data=docs,
                    message="Data Berhasil Di Update"]}
               
                callback(data)
            })
        })
    },
    readClientAllHandlerData: (callback)=> {
        DB.connect(function(err,client,done){
            var data =''
            let adrs=''
            if(err){
                data=err
            }
            
            client.query('select id,name from x_client where is_delete=false',function(err,result){
                done()
                if(err){
                    data=err
                }
                else{ data=result.rows}
                callback(data)
            })
        })
    },
    updateClientAllHandlerData: (callback,docs)=>{
        DB.connect(function(err,client,done){
            var data =''
            let adrs=''
            if(err){
                data=err
            }
            const query ={
                text:'update x_resource_project set modified_by=($1),modified_on=($2),client_id=($3),location=($4),department=($5),pic_name=($6),project_name=($7),start_project=($8),end_project=($9),project_role=($10),project_phase=($11),project_description=($12),project_technology=($13),main_task=($14) where id=($15)',
                values:[docs.modified_by,docs.modified_on,docs.client_id,docs.location,docs.department,docs.pic_name,docs.project_name,docs.start_project,docs.end_project,docs.project_role,docs.project_phase,docs.project_description,docs.project_technology,docs.main_task,docs.id]
            }
            client.query(query,function(err,result){
                done()
                if(err){
                    data=err
                }
                else{ data=[data=docs,
                    message="Data Berhasil Di Update"]}
               
                callback(data)
            })
        })
    },
    tambahClientAllHandlerData: (callback,docs)=>{
        DB.connect(function(err,client,done){
            var data =''
            let adrs=''
            if(err){
                data=err
            }
            const query ={
                text:'insert into x_resource_project (created_by,created_on,client_id,location,department,pic_name,project_name,start_project,end_project,project_role,project_phase,project_description,project_technology,main_task,is_delete) values($1,$2,$3,$4,$5,$6,$7,$8,$9,$10,$11,$12,$13,$14,false)',
                values:[docs.created_by,docs.created_on,docs.client_id,docs.location,docs.department,docs.pic_name,docs.project_name,docs.start_project,docs.end_project,docs.project_role,docs.project_phase,docs.project_description,docs.project_technology,docs.main_task]
            }
            client.query(query,function(err,result){
                done()
                if(err){
                    data=err
                }
                else{ data=[data=docs,
                    message="Data Berhasil Di tambah"]}
               
                callback(data)
            })
        })
    },
    readResourceFindAllHandlerData: (callback,docs)=>{
        DB.connect(function(err,client,done){
            var data =''
            let adrs=''
            if(err){
                data=err
            }
            const query ={
                text:'select x_resource_project.*,name from x_resource_project inner join x_client on x_resource_project.client_id = x_client.id where x_resource_project.is_delete=false and start_project>=($1) and end_project<=($2) or x_resource_project.is_delete=false and end_project>=($1)  and start_project<=($1) or x_resource_project.is_delete=false and end_project>=($2) and start_project<=($2) or x_resource_project.is_delete=false and start_project<=($1) and end_project>=($2)',
                values:[docs.start_project,docs.end_project]
            }
            client.query(query,function(err,result){
                done()
                if(err){
                    data=err;
                }else{
                    for (i=0;i<result.rows.length;i++){
                        const options = {  month: '2-digit', day: '2-digit' ,year: 'numeric'}
                        let exptoken = (result.rows[i].start_project).toLocaleDateString(undefined, options)

                        result.rows[i].start_project = exptoken
                    }
                    for (j=0;j<result.rows.length;j++){
                        const options = {  month: '2-digit', day: '2-digit',year: 'numeric' }
                        let exptoken = (result.rows[j].end_project).toLocaleDateString(undefined, options)

                        result.rows[j].end_project = exptoken
                    }
                    data=result.rows
                }
                callback(data)
            })
        })
    }
}
module.exports = dt