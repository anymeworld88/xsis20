const ResponseHelper = require ('../Helpers/responseHelper')
const dtl = require ('../DataLayer/dt')
const Biodata_BL ={
        readBiodataAllHandler:(req,res,next)=>{
        dtl.readBiodataAllHandlerData(function(items){
            ResponseHelper.sendResponse(res,200,items)
        })
    },
    updateBiodataAllHandler: (req,res,next)=>{
        var docs=req.body
        dtl.updateBiodataAllHandlerData(function(items){
            ResponseHelper.sendResponse(res,200,items)
        },docs)
     },
     updateAlamatAllHandler: (req,res,next)=>{
        var docs=req.body
        console.log(req.body)
        dtl.updateAlamatAllHandlerData(function(items){
            ResponseHelper.sendResponse(res,200,items)
        },docs)
     },
     readBiodataAllHandler1:(req,res,next)=>{
        var docs=req.body
        dtl.readBiodataAllHandlerData1(function(items){
            ResponseHelper.sendResponse(res,200,items)
        },docs)
    }, 
    readPendidikanAllHandler:(req,res,next)=>{
        var docs=req.body
        dtl.readPendidikanAllHandlerData(function(items){
            ResponseHelper.sendResponse(res,200,items)
        },docs)
    },
    readPendidikan1AllHandler:(req,res,next)=>{
        var docs=req.body
        dtl.readPendidikan1AllHandlerData(function(items){
            ResponseHelper.sendResponse(res,200,items)
        },docs)
    },
    readPekerjaanAllHandler:(req,res,next)=>{
        var docs=req.body
        dtl.readPekerjaanAllHandlerData(function(items){
            ResponseHelper.sendResponse(res,200,items)
        },docs)
    },
    readKeahlianAllHandler:(req,res,next)=>{
        var docs=req.body
        dtl.readKeahlianAllHandlerData(function(items){
            ResponseHelper.sendResponse(res,200,items)
        },docs)
    },
    readTrainingAllHandler:(req,res,next)=>{
        var docs=req.body
        dtl.readTrainingAllHandlerData(function(items){
            ResponseHelper.sendResponse(res,200,items)
        },docs)
    },
    readSertifikasiAllHandler:(req,res,next)=>{
        var docs=req.body
        dtl.readSertifikasiAllHandlerData(function(items){
            ResponseHelper.sendResponse(res,200,items)
        },docs)
    },
    readReligionAllHandler:(req,res,next)=>{
        dtl.readReligionAllHandlerData(function(items){
            ResponseHelper.sendResponse(res,200,items)
        })
    },
    readIdentityAllHandler:(req,res,next)=>{
        dtl.readIdentityAllHandlerData(function(items){
            ResponseHelper.sendResponse(res,200,items)
        })
    },
    readMaritalAllHandler:(req,res,next)=>{
        dtl.readMaritalAllHandlerData(function(items){
            ResponseHelper.sendResponse(res,200,items)
        })
    },
    readView2AllHandler:(req,res,next)=>{
        var docs=req.body
        dtl.readView2AllHandlerData(function(items){
            ResponseHelper.sendResponse(res,200,items)
        },docs)
    }, 
    readEmailAllHandler:(req,res,next)=>{
        var docs=req.body
        console.log(req.body)
        dtl.readEmailAllHandlerData(function(items){
            ResponseHelper.sendResponse(res,200,items)
        },docs)
    },
    readNoHp1AllHandler:(req,res,next)=>{
        var docs=req.body
        console.log(req.body)
        dtl.readNoHp1AllHandlerData(function(items){
            ResponseHelper.sendResponse(res,200,items)
        },docs)
    },
    readNoIdentity1AllHandler:(req,res,next)=>{
        var docs=req.body
        console.log(req.body)
        dtl.readNoIdentity1AllHandlerData(function(items){
            ResponseHelper.sendResponse(res,200,items)
        },docs)
    },
    readPhotoProfileAllHandler:(req,res,next)=>{
        var docs=req.body
        dtl.readPhotoProfileAllHandlerData(function(items){
            ResponseHelper.sendResponse(res,200,items)
        },docs)
    },
    readResourceAllHandler:(req,res,next)=>{
        dtl.readResourceAllHandlerData(function(items){
            ResponseHelper.sendResponse(res,200,items)
        })
    },
    readResourceDescAllHandler:(req,res,next)=>{
        dtl.readResourceDescAllHandlerData(function(items){
            ResponseHelper.sendResponse(res,200,items)
        })
    },
    readResourceAscAllHandler:(req,res,next)=>{
        dtl.readResourceAscAllHandlerData(function(items){
            ResponseHelper.sendResponse(res,200,items)
        })
    },
    hapusListAllHandler:(req,res,next)=>{
        var docs=req.body
   
        dtl.hapusListAllHandlerData(function(items){
            ResponseHelper.sendResponse(res,200,items)
        },docs)
    },
    readClientAllHandler:(req,res,next)=>{
        dtl.readClientAllHandlerData(function(items){
            ResponseHelper.sendResponse(res,200,items)
        })
    },
    updateClientAllHandler: (req,res,next)=>{
        var docs=req.body
        dtl.updateClientAllHandlerData(function(items){
            ResponseHelper.sendResponse(res,200,items)
        },docs)
     },
     tambahClientAllHandler: (req,res,next)=>{
        var docs=req.body
        console.log(req.body)
        dtl.tambahClientAllHandlerData(function(items){
            ResponseHelper.sendResponse(res,200,items)
        },docs)
     },
     readResourceFindAllHandler: (req,res,next)=>{
        var docs=req.body
        console.log(req.body)
        dtl.readResourceFindAllHandlerData(function(items){
            ResponseHelper.sendResponse(res,200,items)
        },docs)
     }
}
module.exports= Biodata_BL