const BiodataLogic = require('../Biodata/Biodata_BL.js')
module.exports = exports = function(server){
    server.get('/api/lihatbiodata',BiodataLogic.readBiodataAllHandler),
    server.put('/api/editbiodata',BiodataLogic.updateBiodataAllHandler),
    server.put('/api/editalamat',BiodataLogic.updateAlamatAllHandler),
    server.put('/api/lihatbiodataa',BiodataLogic.readBiodataAllHandler1),
    server.put('/api/pendidikan',BiodataLogic.readPendidikanAllHandler),
    server.put('/api/pekerjaan',BiodataLogic.readPekerjaanAllHandler),
    server.put('/api/keahlian',BiodataLogic.readKeahlianAllHandler),
    server.put('/api/training',BiodataLogic.readTrainingAllHandler),
    server.put('/api/sertifikasi',BiodataLogic.readSertifikasiAllHandler),
    server.get('/api/religion',BiodataLogic.readReligionAllHandler),
    server.get('/api/identity',BiodataLogic.readIdentityAllHandler),
    server.get('/api/marital',BiodataLogic.readMaritalAllHandler),
    server.put('/api/view2',BiodataLogic.readView2AllHandler),
    server.put('/api/email',BiodataLogic.readEmailAllHandler),
    server.put('/api/photoprofile',BiodataLogic.readPhotoProfileAllHandler),
    server.get('/api/lihatresource',BiodataLogic.readResourceAllHandler),
    server.get('/api/lihatresourcedesc',BiodataLogic.readResourceDescAllHandler),
    server.get('/api/lihatresourceasc',BiodataLogic.readResourceAscAllHandler),
    server.put('/api/hapuslist',BiodataLogic.hapusListAllHandler),
    server.get('/api/client',BiodataLogic.readClientAllHandler),
    server.put('/api/editclient',BiodataLogic.updateClientAllHandler),
    server.post('/api/tambahclient',BiodataLogic.tambahClientAllHandler),
    server.put('/api/pendidikan1',BiodataLogic.readPendidikan1AllHandler),
    server.put('/api/nohp1',BiodataLogic.readNoHp1AllHandler),
    server.put('/api/noidentity1',BiodataLogic.readNoIdentity1AllHandler),
    server.put('/api/resourcefind',BiodataLogic.readResourceFindAllHandler)



}